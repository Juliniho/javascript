// CREAR COMPONENTES 'encima de la instancia de VUE'
/**
 * Vue.component('nombre de la etiqueta-componente',{
 *     objecto => template : 'etiqueta html  etiqueta vuejs etiqueta html'
 * })
 */
Vue.component('son', {
    // -- estatico --
    // template: '<h1> SALUDO ESTATICO </h1>',
    // -- dinamico -- !importante 
    // usando el template html es6-string html VS para ver el codigo como html
    template: // html 
        `
    <div>
       <p>{{text}}</p>
       <p>Dato desde el Padre : {{dato}}</p>
       <p>Dato enviado al Padre : {{dad}}</p>

    </div>
    `,
    // Data ha de ser un funcion que devuelve un objeto, no como la instancia new Vue
    data() {
        return {
            text: 'Componente hijo',
            dad: 'Juan Cardenas'
        }
    },

    // ARRAY o lista de los valores enviados de un COMPONENTE (p.e padre -> hijo) a otro COMPONENTE
    // se utiliza como una data
    props: ['dato'],

    // Se ejecuta una vez que el DOM esta listo, esto se utilizara para comunicar COMPONENTE del hijo -> Padre
    // PROBLEMA ES QUE ESTO ES LA COMUNICACION ENTRE HIJO Y PADRE, POR QUE LAS VARIABLES SE VUELVEN LOCALES Y NO ACTUALIZAN AMBOS COMPONENTES
    mounted: function() {
        // emite un evento $emit(nombre del evento,variable o valor)
        this.$emit('emiteDesdeHijo', this.dad);
    }

})