// CREAR COMPONENTES 'encima de la instancia de VUE'
/**
 * Vue.component('nombre de la etiqueta-componente',{
 *     objecto => template : 'etiqueta html  etiqueta vuejs etiqueta html'
 * })
 */
Vue.component('saludo',{
    // -- estatico --
    // template: '<h1> SALUDO ESTATICO </h1>',
    // -- dinamico -- !importante 
    // usando el template html es6-string html VS para ver el codigo como html
    template: // html 
    `
    <div>
        <h3> {{titulo}} </h3>
        <h4> {{subtitulo}} </h4>
    </div>
    `,
    // Data ha de ser un funcion que devuelve un objeto, no como la instancia new Vue
    data () {
        return {
            titulo : 'hola desde componente creado por Vue.component',
            subtitulo : ' seguimos en el componente ' 
        }
    }
})

