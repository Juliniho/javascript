// CREAR COMPONENTES 'encima de la instancia de VUE'
/**
 * Vue.component('nombre de la etiqueta-componente',{
 *     objecto => template : 'etiqueta html  etiqueta vuejs etiqueta html'
 * })
 */
Vue.component('contador',{
    // -- estatico --
    // template: '<h1> SALUDO ESTATICO </h1>',
    // -- dinamico -- !importante 
    // usando el template html es6-string html VS para ver el codigo como html
    template: // html 
    `
    <div>
        <p>contador por componente : {{contador}}</p>
        <a class="waves-effect waves-light btn" v-on:click="contador++">+</a>
        <a class="waves-effect waves-light btn" v-on:click="contador--">-</a>
    </div>
    `,
    // Data ha de ser un funcion que devuelve un objeto, no como la instancia new Vue
    data () {
        return {
            contador : 0
        }
    }
})

