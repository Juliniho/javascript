// CREAR COMPONENTES 'encima de la instancia de VUE'
/**
 * Vue.component('nombre de la etiqueta-componente',{
 *     objecto => template : 'etiqueta html  etiqueta vuejs etiqueta html'
 * })
 */
Vue.component('dad',{
    // -- estatico --
    // template: '<h1> SALUDO ESTATICO </h1>',
    // -- dinamico -- !importante 
    // usando el template html es6-string html VS para ver el codigo como html

    // PROPS  : en la etiqueta "<son valor" es la variable que le envia de PADRE -> HIJO el dato,valor,etc a un ARRAY PROPS AL HIJO

    template: // html 
    `
    <div>
        <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">C.PADRE : {{valor}} {{capturaDesdeHijo}}</h5>

                    <son :dato = "valor" class="card-text" @emiteDesdeHijo="capturaDesdeHijo = $event"  ></son>

                    <a class="btn btn-primary" @click="valor++">+</a>
                </div>
        </div>
    </div>
    `,
    // Data ha de ser un funcion que devuelve un objeto, no como la instancia new Vue
    data () {
        return {
            // dato que se envia tambien al componente hijo
            valor : 5,
            capturaDesdeHijo : ''
        }
    }
})
