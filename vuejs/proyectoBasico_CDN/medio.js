// INSTANCIA un objeto vuejs con new Vue({object})
const app = new Vue({
    // METODO EL dentro de app va a ser gestionado con vue
    el : '#app',

    // METODO DATA los datos se trabajan como propiedades
    // se puede ver desde el f12 en consola desde el navegador app.nuevoItem
    data : {
        h3_0 : 'CSS dinamico :class',
        // Hacemos que el bootstrap sea dinamico, porque lo que iria en una clase,
        // acabamos meterlo en una lista :class
        fondo : 'bg-danger',
        color : false,
        h3_1 : 'Componentes',
        h3_2 : 'Comunicación entre COMPONENTES PROPS'
    },

    // Metodos
    methods:{
        // tambien se puede declarar un metodo asi
        // agregarItem () {}
        // Para el array
        
    },

    // Computa cambios en las propiedades en caliente de data:
    computed : {
        
    }
})

