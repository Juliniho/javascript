// instanciamos un objeto vuejs con new Vue({object})
const app = new Vue({
    // METODO EL dentro de app va a ser gestionado con vue
    el: "#app",

    // METODO DATA los datos se trabajan como propiedades
    // se puede ver desde el f12 en consola desde el navegador app.nuevoItem
    data: {
        h3_1: "Propiedades COMPUTADAS  - COMPUTED",
        titulo: "Basico VUEJS",
        h3_0: "Propiedades y Metodos EL - DATA - methods",
        items: ["manzana", "pera", "naranja"],
        objectos: [
            { nombre: "manzana", cantidad: 12 },
            { nombre: "pera", cantidad: 11 },
            { nombre: "naranja", cantidad: 14 }
        ],
        text0: "Vemos el total de cantidades en objetos",
        nuevoItem: "",
        total: 0,
        frase: "",
        porciento: "%",
        contador: 50
        /**
         * ejmplo CSS <div v-bind:style="styleObject"></div> 
         * data: {
                    styleObject: {
                        color: 'red',
                        fontSize: '13px'
                    }
                 }
         */
    },

    // Metodos
    methods: {
        // tambien se puede declarar un metodo asi
        // agregarItem () {}
        // Para el array
        agregarItem: function () {
            // this se accede a data
            this.items.push(this.nuevoItem);
            console.log("diste clic y enviaste al metodo agregarItem");
            this.nuevoItem = "";
        },
        // Para el objeto
        agregarOtroItem() {
            this.objectos.push({ nombre: this.nuevoItem, cantidad: 2 });
            this.nuevoItem = "";
        }
    },

    // Computa cambios en las propiedades DE DATA:
    computed: {
        sumarCantidades() {
            // reduce permite iterar 1 a 1 objeto de forma que se pueda sumar, restar etc cada 1 de los objetos
            var t = this.objectos.reduce((total, item) => {
                return total + item.cantidad;
            }, 0);
            this.total = t;
            return this.total;
        },

        invertirFrase: function () {
            return this.frase.split("").reverse().join();
        }
    }
});
