Vue.component('datos',{
    template: // html 
    `
    <div>
        <p> {{dato}} => {{ $store.state.numero }}</p>
        <pulsador></pulsador>
    </div>
    `,
    data () {
        return {
            dato : 'Componente : datos'
        }
    }
});

Vue.component('pulsador',{
    template: // html 
    `
    <div>
        <p> Componente : pulsador => {{ $store.state.numero}}</p>
        <button type="button" class="btn btn-primary" @click="$store.commit('incrementa')">+</button>
    </div>
    `,
    data () {
        return {
    
        }
    }
});

Vue.component('computado',{
    template: // html 
    `
    <div>
        <p> Componente : computado => {{ callnumber }}</p>
    </div>
    `,
    data () {
        return {
    
        }
    },

    computed: {
        // SE PUEDE UNA PROPIEDAD COMPUTADA PARA VUEX, para evitar $store.state.NOMBRE DEL DATO
        // recordar que los metodos computados tienen que devolver un return si o si
        callnumber (){
            // no se utiliza el simbolo en codigo js , solo para html $store.state.numero
            return store.state.numero      
        },
    }
});

// ======================================================================================================================================================



Vue.component('mapeando',{
    template: // html 
    `
    <div>
        <p> Componente : mapeando => {{ numero }}</p>
    </div>
    `,
    data () {
        return {
    
        }
    },

    computed: {
        // LLamos directamente a la import { mapState } from 'vuex' y mapeamos el atributo del state entre comillas
            ...Vuex.mapState(['numero']),
        },
    }
);

// ======================================================================================================================================================


Vue.component('mutaciones',{
    template: // html 
    `
    <div>
        <p> Componente : mutaciones => {{ numero }}</p>
        <button type="button" class="btn btn-primary" @click="incrementa">+</button>
        <button type="button" class="btn btn-primary" @click="decrementa">-</button>
    </div>
    `,
    data () {
        return {
    
        }
    },

    computed: {
        // LLamos directamente a la import { mapState } from 'vuex' y mapeamos el atributo del state entre comillas
            ...Vuex.mapState(['numero']),
        },
    methods:{
            ...Vuex.mapMutations(['incrementa','decrementa']),
    }
}
);

// ======================================================================================================================================================


Vue.component('acciones',{
    template: // html 
    `
    <div>
        <p> Componente : Acciones </p>
        <p> Aqui el componente por medio de la vuex, cogera datos de un .json y los printaremos con un boton. </p>
        <p> Como los datos estan implementados en vuex, se puede llamar desde cualquier componente.</p>
        <ul class="list-group" v-for="item of data">
        <li class="list-group-item" > {{item.name}} </li>
        </ul>

        <button type="button" class="btn btn-primary" @click="obtenerJson"> .json</button>

    </div>
    `,

    computed: {
        // LLamos directamente a la import { mapState } from 'vuex' y mapeamos el atributo del state entre comillas
            ...Vuex.mapState(['numero','data']),
        },
    methods:{
            ...Vuex.mapMutations(['incrementa','decrementa']),
            ...Vuex.mapActions(['obtenerJson']),
    }
}
);

// ======================================================================================================================================================

// INSTANCIA VUEX, variables disponibles para culaquier componente
const store = new Vuex.Store({
    // definimos propiedades, variables, datos
    state : {
      numero : 10,
      n : 2,
      data : []
    },
    // definimos metodos
    mutations: {
        // como parametro para no usar el this(state)
        incrementa () {
          this.state.numero++;
        },

        decrementa (state,n) {
        state.numero -= state.n ;
        },

        cogerData(state, dbObtenerJson){
            state.data = dbObtenerJson
        }
    },

    // BACKEND
    actions : {
        obtenerJson : async function ( { commit } ){
            // fetch espera una url, await espera a async
            const db = await fetch('bd.json')
            const info = await db.json()
            // commit le pasara los datos que se han guardado 'info' al metodo cogerData, que lo tiene como segundo parametro 'dbObtenerJson'== 'info'
            // cogerData :: procesara los datos para cogerlos desde una funcion en las mutaciones y al final desde el componente mapeamos la funcion cogerData
            commit('cogerData',info)
        }
    }
});

const app = new Vue({

    el: "#app",
    
    // Definimos la biblioteca VUEX en la instancia VUE, tambien va solo con 1 solo store
    store : store,


    data: {
        
    },

    methods: {
        
    },

    computed: {
        // SE PUEDE UNA PROPIEDAD COMPUTADA PARA VUEX, recordar que los metodos computados tienen que devolver un return si o si, lo utizaremos en el template de datos
        numero(){
            // no se utiliza el simbolo en codigo js , solo para html $store.state.numero
            return store.state.numero      
        },
    }
});
