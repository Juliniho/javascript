// instanciamos un objeto vuejs con new Vue({object})
const app = new Vue({
    // METODO EL dentro de app va a ser gestionado con vue
    el: "#app",

    // METODO DATA los datos se trabajan como propiedades
    // se puede ver desde el f12 en consola desde el navegador app.nuevoItem
    data: {
        h3: "GYM",
        textoInput: "",
        listInputs: []
    },

    // Metodos
    methods: {
        // tambien se puede declarar un metodo asi
        // agregarItem () {}
        // Para el array
        agregarItem() {
            this.listInputs.push({
                nombre: this.textoInput,
                estado: false
            });
            this.textoInput = "";
            // nombre var para identificar en el localstorage
            // nombre o key , dato o value
            localStorage.setItem("gym-data", JSON.stringify(this.listInputs));
        },

        deleteItem(index) {
            // splice (indice,cantidad elementos a eliminar)
            this.listInputs.splice(index, 1);
            localStorage.setItem("gym-data", JSON.stringify(this.listInputs));
        },

        cambiarEstado: function(index) {
            this.listInputs[index].estado = true;
            localStorage.setItem("gym-data", JSON.stringify(this.listInputs));
        },

        // obtener el dato del localstorage
        showData() {

            // si existe dale!!
            if (localStorage.getItem("gym-data")) {
                let dato = JSON.parse(localStorage.getItem("gym-data"))
            } else {
                console.log("no existen entradas!! en el localStorage")
            }



        }
    },

    // Computa cambios en las propiedades en caliente de data:
    computed: {},

    // PRIMER METODO QUE CARGA cuando vuejs lee #app
    /**
     * Llamada sincrónica después de la creación de la instancia. En esta etapa,
     * la instancia ha terminado de procesar las opciones, lo que significa
     * que se ha configurado lo siguiente:
     * observación de datos, propiedades calculadas, métodos, llamadas de seguimiento/evento.
     * data observation, computed properties, methods, watch/event callbacks
     * Sin embargo, la fase de montaje no se ha iniciado y la propiedad $el no estará disponible todavía.
     * @link diagrama ciclo de vida vuejs: https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram
     */

    created: function() {
        /**
         * @param localStorage.getItem (clave o nombre con que se guarda en el localStore)
         * JSON.PARSE, pasarlo object => JSON
         */
        var data = JSON.parse(localStorage.getItem("gym-data"));
        console.log(`valor del method::created : ${JSON.stringify(data)}`);

        // Control
        if (data === null) {
            this.listInputs = [];
            console.log(`Estamos en el if ${data}`);
        } else {
            this.listInputs = data;
            console.log(`Estamos en el if ${this.listInputs}`);
        }
    }
});