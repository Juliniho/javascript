# Ciclo de vida Vuejs
>https://es.vuejs.org/v2/guide/instance.html#Diagrama-del-Ciclo-de-vida

Donde se encuentran :: dentro de la instancia

new Vue() -> beforeCreate -> Created -> 

const app = new Vue({
    `beforeCreate`
    el: '#app',
    `created`
    data: {    
    },
    methods: {
        destruir () {
            this.$destroy()
        }
    },
    computed: {
    },
    
    // se ejecuta primero antes de todo
    beforeCreate(){ console.log(beforeCreate())}

    // se ejecuta, despues de leer data,methods,computed, NO el:
    created(){ console.log(created())}

    // se ejecuta antes de insertar la informacion en el DOM, en html {{}}
    beforeMount(){ console.log(beforeMount())}

    // se ejecuta despues de leer la informacion
    mount(){ console.log(mount())}



    // se ejecuta al detectar un cambio en la instancia (en el codigo js)
    beforeUpdate(){ console.log(beforeUpdate())}

    // se ejecuta cuando hace un cambio, en el DOM (html)
    update(){ console.log(update())}



    // destruye la instacia de vue
    beforedestroy(){ console.log(beforedestroy())}
    
    destroyed(){ console.log(beforeUpdate())}
})

# Componentes = separar en secciones de nuestra app

# ciclo de vida vuex


