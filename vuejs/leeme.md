# INSTALACION

https://vuejs.org/

> instalacion por CDN

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>

> instalacion Vue CLI docs

    https://cli.vuejs.org/guide/installation.html

    npm install -g @vue/cli
    vue --version

    vue create my-project (creacion por consola -> manual->babel/vuex/linter->intro->intro->dedicate->N(haremos mas instalaciones))

    cd nameProject
    npm run serve

> instalacion Vue vue ui

    interfast grafica


# CONFIGURACIONES

## Bootstrap
para configuraciones de los paquetes npm en el file :

> src/main.js

instalar bootstrap en nuestro proyecto : la gracia es poder usar etiquetas en lugar del CLASS="blabla"
https://getbootstrap.com/docs/4.4/components


> https://bootstrap-vue.js.org/docs/    

    npm install vue bootstrap-vue bootstrap
    
    'intro main.js'
    // Then, register BootstrapVue in your app entry point:
    import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
    // Install BootstrapVue
    Vue.use(BootstrapVue)
    // import Bootstrap and BootstrapVue css files:
    import 'bootstrap/dist/css/bootstrap.css'
    import 'bootstrap-vue/dist/bootstrap-vue.css'
> 

# Vue

## router-link
> https://router.vuejs.org/api/#router-link

        <!-- literal string -->
        <router-link to="home">Home</router-link>
        <!-- renders to -->
        <a href="home">Home</a>

        <!-- javascript expression using `v-bind` -->
        <router-link v-bind:to="'home'">Home</router-link>

        <!-- Omitting `v-bind` is fine, just as binding any other prop -->
        <router-link :to="'home'">Home</router-link>

        <!-- same as above -->
        <router-link :to="{ path: 'home' }">Home</router-link>

        <!-- named route -->
        <router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>

        <!-- with query, resulting in `/register?plan=private` -->
        <router-link :to="{ path: 'register', query: { plan: 'private' }}"
        >Register</router-link
        
# Util Vuejs

* se repite el parrafo 5 veces
````
<p class="my-4" v-for="i in 5" :key="i">Cras a</p>
````