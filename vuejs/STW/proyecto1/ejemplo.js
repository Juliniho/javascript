Vue.component('redundant-checkbox', {
  data: function() {
    return {
      checkbox1: false,
      checkbox2: false,
    };
  },
  props: ['value'],
  watch: {
    checkbox1: function() {
      this.$emit('input', this.checkbox1 && this.checkbox2);
    },
    checkbox2: function() {
      this.$emit('input', this.checkbox1 && this.checkbox2);
    },
  },
  template: `<span>
    <input type="checkbox" v-model="checkbox1"> {{checkbox1}}
    <input type="checkbox" v-model="checkbox2"> {{checkbox2}}
  </span>`,
});



var vm = new Vue({
  el: "#app",
  data: {
    checkbox: false
  },
  template: `<div>
    <redundant-checkbox v-on:input="checkbox=$event"></redundant-checkbox>
    {{checkbox}}
    </div>`,
});



/*
v-model="checkbox"
v-bind:value="checkbox"
v-on:input="checkbox=$event"*/




/*
Vue.component('secure-button', {
  data: function() {
    return {
      enabled: false,
    }
  },
  methods: {
    clickButton: function() {
      this.enabled = false;
      this.$emit('secure-click', 'yes');
    }
  },
  template: `<div style="display:inline-block;padding:10px;margin:10px;border:solid red;">
  <button v-on:click="clickButton()" v-bind:disabled="! enabled"><slot></slot></button>
<label><input type="checkbox" v-model="enabled">Enable</label></div>`,
});



Vue.component('fancy-chronometer', {
  data: function() {
    return {
      time: 0,
    }
  },
  props: ['chronoLabel'],
  methods: {
    clickButton: function() {
      this.time = 0;
    }
  },
  template: `<div style="display:inline-block;padding:10px;margin:10px;border:solid;">
  <span v-if="chronoLabel">{{chronoLabel}}: </span>
  {{time}}
  <secure-button v-on:secure-click="$event == 'yes' ? clickButton() : ''">Reset</secure-button>



  v-model="variable"



  v-bind:value = "variable"
  v-on:input = "variable = $event"



  </div>`,
  created: function() {
    this.label
    setInterval(() => this.time++, 1000);
  }
});



var vm = new Vue({
  el: "#app",
  data: {
    label2: "Hola"
  },
  template: `<div>
    <fancy-chronometer chrono-label="Crono 1"></fancy-chronometer>
    <fancy-chronometer v-bind:chrono-label="label2"></fancy-chronometer>
    <fancy-chronometer></fancy-chronometer>
    <secure-button>No fa res</secure-button>
    </div>`,
});
*/
