let options = {
    el: '#app',

    // propiedades
    data: {

        litros: 0,
        cm3: 0,


    },

    methods: {
        conversion() {
            return this.litros * 3000
        }
    },

    // observador personalizado,  operaciones asíncronas
    watch: {

        litros: function() {
            this.cm3 = this.litros * 3000
        },


    },

    template: `
    <div>
    <span> Cambiar litros a centimetros cubicos :  </span> <br>

    <!-- v-model permite interpretar los valores de vuejs = two way-->
    Litros : <input type="text" v-model="litros" > <br>

    <!-- v-bind permite interpretar los valores de vuejs = one way-->
    cm^3 con methods : <input type="text" v-bind:value="conversion()">  <br>

   <!-- watcher observa el valor de la variable y actualiza, si es un objecto -->
   <!-- form.name form es el objeto y name la propiedad-->
    cm^3 con watcher : <input type="text" v-model="cm3"> <br><br>


    <button v-on:click="litros=0">reinicia</button>



    </div>`,
}

let vm = new Vue(options)