import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Then, register BootstrapVue in your app entry point:
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// Install BootstrapVue
Vue.use(BootstrapVue)
// import Bootstrap and BootstrapVue css files:
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
