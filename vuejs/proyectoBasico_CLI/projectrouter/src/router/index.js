import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/responsive',
    name: 'grid',
    component: () => import(/* webpackChunkName: "about" */ '../views/Responsive.vue')
  },
  // ruta estatica
  {
    path: '/servicios',
    name: 'servicios',
    component: () => import(/* webpackChunkName: "servicios" */ '../views/Servicios.vue')
  },
  {
    path: '/tarjetas',
    name: 'card',
    component: () => import(/* webpackChunkName: "servicios" */ '../views/Card.vue')
  },
  {
    path: '/tarjetas',
    name: 'card',
    component: () => import(/* webpackChunkName: "servicios" */ '../views/Card.vue')
  },
  // ruta dinamica
  {
    path: '/form',
    name: 'formulario',
    component: () => import(/* webpackChunkName: "fotos" */ '../views/Form.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
