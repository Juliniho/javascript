import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    frutas: [
      { nombre: "manzana", qt: 0 },
      { nombre: "pera", qt: 0 },
      { nombre: "piña", qt: 0 },
      { nombre: "uvas", qt: 0 }
    ]
  },
  mutations: {
    aumentar : function (state,index){
      state.frutas[index].qt++;
    },
    reset (){
      this.state.frutas.map(resetea =>{
        resetea.qt = 0;
      })
    }

  },
  actions: {
  },
  modules: {
  }
})
