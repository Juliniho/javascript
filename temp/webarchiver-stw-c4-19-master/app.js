var http = require('http');
var request = require('request');
var cheerio = require ('cheerio');
var stream = require('stream');
var fs = require('fs');
var path = require('path');
var url = require('url');
var archiver = require('archiver');
var zip = archiver('zip');


function getTransformStream(url, recLevel, replaceManager, downloadedFiles, doCrawlAndDownloadResource) {
  var transformStream = new stream.Transform();
  var buffer='';

  transformStream._transform = function(chunk, encoding, callback) {    
    buffer += chunk.toString();
    callback();
  };

  transformStream._flush = function(callback){
    this.push(transformStream._replace(buffer));
    callback();
  }

  transformStream._replace = function(chunk){
      $ = cheerio.load(chunk);
      $('a').each(function (i, link){
        var newUrl = $(this).attr('href'); 
        var downloadableURL = URLManager.getDownloadableURL(url,newUrl);
        var newUrlName = replaceManager.lookupName(downloadableURL);
        $(this).attr('href', newUrlName);
        //ojo
        doCrawlAndDownloadResource(downloadableURL,
          recLevel - 1, replaceManager, newUrlName, downloadedFiles); 
        
      }); //end $a.each
      return $.html();
  }; 
  
  return transformStream;  
}

function URLManager(){
  
}
  URLManager.getResourceExtension =function(url){
    
    var objExt =  "";
    objExt = path.extname(url)== "" ? ".html": path.extname(url);
    return objExt;
  }

  URLManager.getDownloadableURL = function(urlParent, href){

    if(href == undefined){
      href = "";
    }
    return url.resolve(urlParent, href);
  }


function replaceManager(maxFiles){
  this._fileCounter = 0;
  this._replaceMap = [];
  this._NOT_FOUND_FILE = "_404.html";
  
  this.lookupName = function(_url){
    if(Object.keys(this._replaceMap).length === 0){
      console.log("vacio----");
      this._replaceMap[_url] = 'index.html';
      this._fileCounter++;
    }else{
      if(!(_url in this._replaceMap) &&  this._fileCounter < maxFiles ){
        console.log("Found");
        this._replaceMap[_url] = this._fileCounter + URLManager.getResourceExtension(_url);
        console.log("---- " +this._replaceMap[_url]);
        console.log("extension:   " + URLManager.getResourceExtension(_url));
        this._fileCounter++;
      } else{ 
        if(!(_url in this._replaceMap)){
        this._replaceMap[_url] = this._NOT_FOUND_FILE;
        }
      }
      }
      return this._replaceMap[_url];
    }
  }
  
function startCrawling(req, res, queryReq){
  var url = queryReq.url;
  var maxFiles = queryReq.maxFiles;
  var recLevel = queryReq.recLevel;
  var downloadedFiles = [];
  var count =  0 ;
  var finish = 0;
  var replace = new replaceManager(maxFiles);
  replace.lookupName(url);

  //console.log(replace._replaceMap[url]);
  res.writeHead(200, {'Content-Type': 'application/zip','Content-Disposition' : 'attachment; filename=files.zip'});
  zip.pipe(res);

  var doCrawlAndDownloadResource = function(url, recLevel,replaceManager, entryName, downloadedFiles){
    console.log("Do Crawling");
    console.log(url);
    
    if( recLevel >= 0 && !downloadedFiles.includes(entryName)  && downloadedFiles.length< maxFiles){
      console.log((entryName in downloadedFiles));
      downloadedFiles.push(entryName);
      count++;
      var transform = getTransformStream(url, recLevel, replaceManager, downloadedFiles, doCrawlAndDownloadResource);
      zip.append(request.get(url)
      .pipe(transform)
      .on('finish',() =>{
          finish++;
          if(count == finish){
            zip.finalize()
          }
        }),{name : entryName});
        
  }
}
 
  doCrawlAndDownloadResource(url, recLevel, replace, replace._replaceMap[url], downloadedFiles);
}


function routeRequests(req, res){
  console.log("req.url: " + req.url);
    //req stream lectura, peticiones que hace el cliente
    // res stream escritura, respuestas hacia el cliente
  // TODO
  if(req.url == '/'){
    res.writeHead(200, {'Content-Type': 'text/html'});
    fs.createReadStream('./view/index.html').pipe(res);
  }

  if(url.parse(req.url).pathname == '/crawler'){
    console.log('=============> formulario ')
    var urlObject = url.parse(req.url, true);
    var urlQuery = urlObject.query;
    startCrawling(req,res,urlQuery)
  }
}

http.createServer(routeRequests).listen(8081);
