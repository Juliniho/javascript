/******************************************************************
 *********************** PROBLEM 1 ********************************
 ******************************************************************/
var f1 = function(a) {
  console.log(a);
}
//f1(3);


/******************************************************************
 *********************** PROBLEM 2 ********************************
 ******************************************************************/
var f2 = function(a) {
  return (a >= 0) ? 2 * a : -1;
}
//console.log(f2(3));
//console.log(f2(-2));


/******************************************************************
 *********************** PROBLEM 3 ********************************
 ******************************************************************/
 var f3 = function(llista) {
   function suma(x) {
     return (f2(x) + 23);
   }
   return llista.map(suma);
 }
 //console.log(f3([1,2,3]));


 /******************************************************************
  *********************** PROBLEM 4 ********************************
  ******************************************************************/
var printaki = function() {
  console.log('aqui');
}
//console.printaki = printaki;
//console.printaki();


/******************************************************************
 *********************** PROBLEM 5 ********************************
 ******************************************************************/
var f4 = function(a, b) {
  return a + b;
}
llistaA = [1,2,3,4];
llistaB = llistaA.map(function(x) {
  return f4(x, 23);
});
//console.log(llistaB);


/******************************************************************
 *********************** PROBLEM 6 ********************************
 ******************************************************************/
 var f5 = function(a, b, c) {
   c(b(a));
 }
 //f5(1, f2, function(r) { console.log(r); });


 /******************************************************************
  *********************** PROBLEM 7 ********************************
  ******************************************************************/
var printaki2 = function() {
  var count = 1;
  return function() {
    console.log('aqui ' + count);
    count++;
  }
}
/*console.printaki2 = printaki2();
console.printaki2(); //aqui 1
console.printaki2(); //aqui 2
console.printaki2(); //aqui 3*/


/******************************************************************
 *********************** PROBLEM 8 ********************************
 ******************************************************************/
var fs = require('fs');
var f6 = function(llista, callback_final) {
  var llistaResultat = [];
  llista.forEach((valor, i) => {
    fs.readFile(llista[i], 'utf-8', function(err, result) {
      llistaResultat[i] = result;
      if (i === llista.length - 1) {
        callback_final(llistaResultat);
      }
    });
  });
}
//f6(['a1.txt', 'a2.txt'], function(result) { console.log(result); });


/******************************************************************
 *********************** PROBLEM 9 ********************************
 ******************************************************************/
var f7 = function(llista, callback_final) {
  var llistaResultat = [];
  llista.forEach((valor, i) => {
    fs.readFile(llista[i], 'utf-8', function(err, result) {
      if (err) {
        throw err;
      }
      else {
        llistaResultat[i] = result;
      }
      if (i === llista.length -1) {
        callback_final(llistaResultat);
      }
    });
  });
}
//f7(['a1.txt', 'a2.txt'], function(result) { console.log(result); });


/******************************************************************
 *********************** PROBLEM 11 *******************************
 ******************************************************************/
 var asyncMap = function(list, f, callback2) {
   var llistaResultat = [];
   list.forEach((valor, i) => {
     f = fs.readFile(list[i], 'utf-8', function(err, result) {
       llistaResultat[i] = result;
       if (err != null) {
         callback2(err, llistaResultat);
       }
       else {
         if (i === list.length - 1) {
           callback2(err, llistaResultat);
         }
       }
     });
   });
 }
 //asyncMap(['a1.txt'], fs.readFile, function(a,b) {console.log(b);});


 /******************************************************************
  *********************** PROBLEM 12 *******************************
  ******************************************************************/
var Counter = function() {
  this.count = 0;
  this.notify;
  this.inc = function() {
    this.count++;
    if (this.notify != null) {
      this.notify(this.count);
    }
  };
}
var o1 = new Counter();
/*o1.notify = null;
o1.inc();

o1.count = 1;
o1.notify = function(a) { console.log(a); };
o1.inc();*/ // result: 2 OK!


/******************************************************************
 *********************** PROBLEM 13 *******************************
 ******************************************************************/
var o2 = (function() {
  var count = 1;
  var notify;

  return {
    inc: function() { count++; return notify(count); },
    count: function() { return count; },
    setNotify: function(f) { notify = f; }
  };
})();
//o2.setNotify(function(a) {console.log(a); });
//o2.inc(); //result: 2 OK!


/******************************************************************
 *********************** PROBLEM 14 *******************************
 ******************************************************************/
function o2() {
  this.count = 1;
  this.notify;
  this.inc = function() { this.count++; return this.notify(count); };
  this.count = function() { return this.count; };
  this.setNotify = function(f) { this.notify = f; };
}
//var o3 = new o2();


/******************************************************************
 *********************** PROBLEM 15 *******************************
 ******************************************************************/
function DecreasingCounter() {
  this.inc = function() { this.count--; };
}
DecreasingCounter.prototype = new Counter();
/*var o4 = new DecreasingCounter();
o4.count = 2;
o4.inc();
console.log(o4.count);*/


/******************************************************************
 *********************** PROBLEM 16 *******************************
 ******************************************************************/
 future = {isDone: false, result: null};
 var readIntoFuture = function(filename) {
   fs.readFile(filename, 'utf-8', function(err, result) {
     if (err) {
       throw err;
     }
     else {
       future.isDone = true;
       future.result = result;
     }
   });
   return future;
 }
 /*future = readIntoFuture('a1.txt');
 console.log(future);
future = readIntoFuture('a1.txt');
setTimeout(function() { console.log(future); }, 1000);*/


/******************************************************************
 *********************** PROBLEM 17 *******************************
 ******************************************************************/
var asyncToFuture = function(f) {
  return function(filename) {
    f(filename, 'utf-8', function(err, result) {
      if (err) {
        throw err;
      }
      else {
        future.isDone = true;
        future.result = result;
      }
    });
    return future;
  }
}
/*readIntoFuture2 = asyncToFuture(fs.readFile);
future = readIntoFuture2('a1.txt');
setTimeout(function() {console.log(future);},1000);

statIntoFuture = asyncToFuture(fs.stat);
future = statIntoFuture('a1.txt');
setTimeout(function() {console.log(future);}, 1000);*/


/******************************************************************
 *********************** PROBLEM 18 *******************************
 ******************************************************************/
enhancedFuture = {
  isDone: false,
  result: null,
  registerCallback: function(c) { this.callback = c; },
  callback: null
};
var asyncToEnhancedFuture = function(f) {
  return function(filename) {
    f(filename, 'utf-8', function(err, result) {
      if (err) { throw err; }
      else {
        enhancedFuture.isDone = true;
        enhancedFuture.result = result;
        if (enhancedFuture.callback != null) {
          enhancedFuture.callback(enhancedFuture);
        }
      }
    });
    return enhancedFuture;
  }
}
/*readIntoEnhancedFuture = asyncToEnhancedFuture(fs.readFile);
enhancedFuture = readIntoEnhancedFuture('a1.txt');
enhancedFuture.registerCallback(function(ef) { console.log(ef); });*/


/******************************************************************
 *********************** PROBLEM 19 *******************************
 ******************************************************************/
var when = function(f1) {
  return {do: f1};
}
/*f1 = function(callback) {fs.readFile('a1.txt', 'utf-8', callback);};
f2 = function(error, result) { console.log(result); };
when(f1).do(f2);*/


/******************************************************************
 *********************** PROBLEM 20 *******************************
 ******************************************************************/
var when2 = function(f1) {
  return {
    and: function(f2) {
      return {
        do: function(f3) {
          f1(function(err1, res1) {
            f2(function(err2, res2) {
              f3(err1, err2, res1, res2);
            });
          });
        }
      }
    }
  }
}
/*f1 = function(callback) {fs.readFile('a1.txt', 'utf-8', callback);};
f2 = function(callback) {fs.readFile('a2.txt', 'utf-8', callback);};
f3 = function(err1,err2,res1,res2) { console.log(res1, res2); };
when2(f1).and(f2).do(f3);*/


/******************************************************************
 *********************** PROBLEM 21 *******************************
 ******************************************************************/
var composer = function(f1, f2) {
 return function(x) {
   return f1(f2(x));
 }
}
/*f1 = function(a) { return a + 1; };
f3 = composer(f1, f1);
console.log(f3(3)); //return: 5 OK!
f4 = function(a) { return a * 3; };
f5 = composer(f3, f4);
console.log(f5(3)); //return: 5 OK!*/


/******************************************************************
 *********************** PROBLEM 22 *******************************
 ******************************************************************/
 var asyncComposer = function(f1, f2) {
   return function(x, callback) {
     f2(x, function(err, result) {
       if (err != null) {
         callback(err, result);
       }
       else {
         f1(result, function(err, result) {
           callback(err, result);
         });
       }
     });
   }
 }
 /*f1 = function(a, callback) {callback(null, a + 1); };
 f3 = asyncComposer(f1, f1);
 f3(3, function(error, result) { console.log(result); });
 f1 = function(a, callback) { callback(null, a + 1); };
 f2 = function(a, callback) { callback("error", ""); };
 f3 = asyncComposer(f1, f2);
 f3(3, function(error, result) { console.log(error, result); });*/


/******************************************************************
 *********************** PROBLEM 24 *******************************
 ******************************************************************/
var antipromise = function(promesa) {
  return new Promise((resolve, reject) => {
    promesa.then(reject, resolve);
  })
}
//antipromise(Promise.reject(0)).then(console.log); // result: 0 -> OK!
//antipromise(Promise.resolve(0)).catch(console.log); // result: 0 -> OK!


/******************************************************************
 *********************** PROBLEM 25 *******************************
 ******************************************************************/
 var promiseToCallback = function(f) {
   return function(x, callback) {
     f(x).then(res => { callback(null, res); },
               err => { callback(err, null);});
   }
 }
var isEven = x => new Promise (
  (resolve, reject) => x % 2 ? reject(x) : resolve(x)
);
/*var isEvenCallback = promiseToCallback(isEven);
isEven(2).then(() => console.log('OK'), () => console.log('KO'));
isEvenCallback(2, (err, res) => console.log(err, res));
isEven(3).then(() => console.log('OK'), () => console.log('KO'));
isEvenCallback(3, (err, res) => console.log(err, res));*/


/******************************************************************
 *********************** PROBLEM 26 *******************************
 ******************************************************************/
 var readToPromise = function(file) {
   return new Promise((resolve, reject) => {
     fs.readFile(file, 'utf-8', function(err, res) {
       if (err) { reject(err); }
       else { resolve(res); }
     });
   });
 }

 /*readToPromise('a1.txt').then(x => console.log('Contents: ', x))
                        .catch(x => console.log('Error: ', x));
readToPromise('notfound.txt').then(x => console.log('Contents: ', x))
                       .catch(x => console.log('Error: ', x));*/


/******************************************************************
 *********************** PROBLEM 27 *******************************
 ******************************************************************/
 var callbackToPromise = function(f) {
   return function(x) {
     return new Promise((resolve, reject) => {
       f(x, 'utf-8', function(err, res) {
         if (err) { reject(err); }
         else { resolve(res); }
       });
     });
   }
 }
 /*readToPromise2 = callbackToPromise(fs.readFile);
 readToPromise2('a1.txt').then(x => console.log('Contents: ', x))
                         .catch(x => console.log('Error: ', x));*/


/******************************************************************
 *********************** PROBLEM 28 *******************************
 ******************************************************************/
enhancedFuture2 = {
 isDone: false,
 result: null,
 registerCallback: function(c) { this.callback = c; },
 callback: null
};
var enhancedFutureToPromise = function(f) {
 return function(file) {
   return new Promise((resolve, reject) => {
     f(file, 'utf-8', function(err, res) {
       if (err) { reject(err); }
       else {
         enhancedFuture2.isDone = true;
         enhancedFuture2.result = res;
         resolve(enhancedFuture2.result);
       }
     });
   });
 }
}
/*readIntoEnhancedFuture = enhancedFutureToPromise(fs.readFile);
enhancedFuture2 = readIntoEnhancedFuture('a1.txt')
  .then(x => console.log('Content: ', x))
  .catch(x => console.log('Error: ', x));*/


/******************************************************************
 *********************** PROBLEM 29 *******************************
 ******************************************************************/
var mergedPromise = function(promesa) {
  return new Promise((resolve, reject) => {
    promesa.then(x => resolve(x))
           .catch(x => resolve(x));
  });
}
//mergedPromise(Promise.resolve(0)).then(console.log); // return: 0 -> OK!
//mergedPromise(Promise.reject(1)).then(console.log); // return: 1 -< OK!


/******************************************************************
 *********************** PROBLEM 30 *******************************
 ******************************************************************/
var functionPromiseComposer = function(f1, f2) {
  return function(x) {
    return new Promise((resolve, reject) => {
      //TODO...
    });
  }
}
/*var f1 = x => new Promise((resolve, reject) => resolve(x + 1));
functionPromiseComposer(f1, f1)(3).then(console.log);
var f3 = x => new Promise((resolve, reject) => reject('always fails'));
functionPromiseComposer(f1, f3)(3).catch(console.log);*/


/******************************************************************
 *********************** PROBLEM 31 *******************************
 ******************************************************************/
var parallelPromise = function(p1, p2) {
  //TODO...
}
/*var p2 = parallelPromise(Promise.resolve(0), Promise.resolve(1));
p2.then(console.log);*/


/******************************************************************
 *********************** PROBLEM 32 *******************************
 ******************************************************************/
var promiseBarrier = function(n) {
  var list = [];
  var functions = []; //Llista on guardarem les funcions que cridarem
  var params = [];
  var counter = 0;

  for(let i = 0; i < n; i++) {
    list[i] = function(x1) {
      counter++;
      return new Promise((resolve, reject) => {
        functions[i] = resolve;
        params[i] = x1;

        if (counter == n) {
          for(let j = 0; j < n; j++) {
            functions[j](params[j]);
          }
        }
      });
    }
  }
  return list;
}

/*var [f1, f2] = promiseBarrier(2);
Promise.resolve(0)
  .then(x => {console.log('c1 s1'); return x; })
  .then(x => {console.log('c1 s2'); return x; })
  .then(x => {console.log('c1 s3'); return x; })
  .then(x => {console.log('c1 s4'); return x; })
  .then(f1);

Promise.resolve(0)
  .then(f2)
  .then(x => {console.log('c2 s1'); return x; })
  .then(x => {console.log('c2 s2'); return x; });*/

var [f1, f2, f3] = promiseBarrier(3);
Promise.resolve(0)
  .then(x => {console.log('c1 s1'); return x; })
  .then(x => {console.log('c1 s2'); return x; })
  .then(x => {console.log('c1 s3'); return x; })
  .then(f1)
  .then(x => {console.log('c1 s4'); return x; });

Promise.resolve(0)
  .then(x => {console.log('c2 s1'); return x; })
  .then(f2)
  .then(x => {console.log('c2 s2'); return x; });

  Promise.resolve(0)
    .then(f3)
    .then(x => {console.log('c3 s1'); return x; })
    .then(x => {console.log('c3 s2'); return x; })
    .then(x => {console.log('c3 s3'); return x; })
