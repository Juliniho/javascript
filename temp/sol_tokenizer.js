function Tokenizer(){
    this.dictionary = [];
    this.run = function(string){
      var self = this;

      string.forEach(function(char){
        if (self.dictionary[char] != undefined){
          self.dictionary[char]();
        }else{
          self.default();
        }
      });
    }
    this.on = function(char, callback){
      this.dictionary[char] = callback;
    }
    this.onDefault = function(callback){
      this.default = callback;
    }
}

function testTokenizer(){
  var t = new Tokenizer();
  var countA = 0;
  var countC = 0;
  var countOther = 0;
  var testString = ['H','o','l','a',' ','c','o','m',' ','a','n','e','u','?'];
  t.on('a', function(){
    countA++;
  });
  t.on('c', function(){
    countC++;
  });  
  t.onDefault(function(){
    countOther++;
  })
  //here goes the code to run the test over testString
  t.run(testString);
  console.log("numero de a's: " + countA);
  console.log("numero de c's: " + countC);
  console.log("numero d'altres caracters: " + countOther);
}

testTokenizer();

