
var http = require('http')
var fs = require('fs')

var queue = [];

var serverFunction = function(request, response) {
	if (request.url == '/') {
		response.writeHead(200, {'Content-Type': 'text/html'})
		fs.createReadStream('index.html').pipe(response)
	} else {
		queue.push({request: request, response: response})
	}
}

http.createServer(serverFunction).listen(8081);

console.log("Server started!")

// ################# TODO #########################

var TokenBucket = function(refill_rate, max_tokens) {
	var tokens = 0;

	/* todo from here */
	setInterval(() => tokens = Math.min(tokens + refill_rate, max_tokens), 1000);

	this.getOneToken = function (callback) {
		if (tokens > 0) {
			tokens --;
			callback(true)
		} else {
			callback(false)
		}
	}
	/* to here */
}


var tokenBucket = new TokenBucket(3, 10);

var realServerFunction = function() {
	/* todo from here */
	if (queue.length == 0)
		return;

	tokenBucket.getOneToken(function(available) {
		if (available) {
			var o = queue.shift(); // donar aquesta linea o documentar

			o.response.writeHead(200, {'Content-Type': 'text/plain'});
			o.response.write(o.request.url);
			o.response.end();

			realServerFunction();
		}
	});

	/* to here */
}

setInterval(realServerFunction, 100);



