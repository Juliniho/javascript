ar http = require('http');
var request = require('request');
var cheerio = require('cheerio');
var stream = require('stream');
var fs = require('fs');
var path = require('path');
var url = require('url');
var archiver = require('archiver');
var zip = archiver("zip");


function getTransformStream(url, recLevel, replaceManager, downloadedFiles, doCrawlAndDownloadResource) {
    console.log("---------------->>>>>>>>> intro getTransformStreame");
    var transformStream = new stream.Transform();
    var buffer = '';

    transformStream._transform = function(chunk, encoding, callback) {
        buffer += chunk.toString();
        callback();
    };

    transformStream._flush = function(callback) {
        this.push(transformStream._replace(buffer));
        callback();
    };

    transformStream._replace = function(chunk) {
        $ = cheerio.load(chunk);
        $('a').each(function(i, link) {
            var newUrl = $(this).attr('href');
            if (newUrl) {
                var downloadableURL = URLManager.getDownloadableURL(url, newUrl);
                var newUrlName = replaceManager.lookupName(downloadableURL);
                $(this).attr('href', newUrlName);

                doCrawlAndDownloadResource(downloadableURL,
                    recLevel - 1, replaceManager, newUrlName, downloadedFiles);
            }

        }); //end $a.each
        return $.html();
    };

    return transformStream;
}

function ReplaceManager(maxFiles) {

    this._maxFiles = maxFiles;
    this._fileCounter = 0;
    this._replaceMap = [];
    this._NOT_FOUNF_FILE = "404.html";

    this.lookupName = function(url) {
        console.log(url in this._replaceMap);
        if (!(url in this._replaceMap) && this._fileCounter < this._maxFiles) {
            var newName = "";
            if (this._fileCounter == 0) {
                newName = "index.html";
                this._fileCounter++;
            } else {
                newName = this._fileCounter + ".html";
                this._fileCounter++;
            }
            this._replaceMap[url] = newName;
            return newName;
        } else {
            return this._NOT_FOUNF_FILE;
        }
    }
}

function URLManager() {}

URLManager.getResourceExtension = function(url) {
    var extension = "";
    if (path.extname(url) == "") {
        extension = ".html";
    } else {
        extension = path.extname(url);
    }
    return extension;
};

URLManager.getDownloadableURL = function(urlParent, href) {
    console.log('entra URLManager.getDownloadableURL')
    const url = require('url');
    return url.resolve(urlParent, href);
};



function startCrawling(req, res, queryReq) {
    console.log("---------------->>>>>>>>> intro startCrawling");
    var url = queryReq.query.url;
    var recLevel = queryReq.query.reclevel;
    var maxFiles = queryReq.query.maxfiles;
    var replaceManager = new ReplaceManager(maxFiles);
    var outputName = replaceManager.lookupName(url);
    var downloadedFiles = [];
    var downloads = 0;
    var counter = 0;
    zip.pipe(res);
    res.writeHead(200, { "Content-Type": "application/zip", "Content-Disposition": "attachment; filename=files.zip" });


    function doCrawlAndDownloadResource(url, recLevel, replaceManager, outputName, downloadedFiles) {
        console.log("---------------->>>>>>>>> intro doCrawlAndDownloadResource");
        console.log(outputName);
        console.log(url);
        if (recLevel >= 0 && !downloadedFiles.includes(url) && downloadedFiles.length < maxFiles) {
            downloadedFiles.push(url);
            counter++;
            var t = getTransformStream(url, recLevel, replaceManager, downloadedFiles, doCrawlAndDownloadResource);
            request.get(url).pipe(t);
            zip.append(t, { name: outputName });
            t.on("finish", function() {
                downloads++;
                if (counter == downloads) {
                    zip.finalize();
                }
            });
        }
    }
    doCrawlAndDownloadResource(url, recLevel, replaceManager, outputName, downloadedFiles);

}



function routeRequests(req, res) {
    var reqGetUrl = url.parse(req.url, true);


    if (reqGetUrl.pathname == '/starCrawling(this)') {
        console.log("---------------->>>>>>>>> intro GET");
        startCrawling(req, res, reqGetUrl);
    } else {
        console.log("---------------->>>>>>>>> index");
        res.writeHead(200, { "Content-Type": "text/html" });
        fs.createReadStream("view/index.html").pipe(res);
    }
}

http.createServer(routeRequests).listen(8081);©
2020 GitHub, Inc.