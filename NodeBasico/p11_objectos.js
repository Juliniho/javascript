console.log('===================================================================================');
console.log(' OBJETOS ');
console.log('===================================================================================');

console.log('===================================================================================');
console.log(' Single::CAPA 1 ');
console.log('===================================================================================');


var person = {
    name : '',
    descriptions : function (){ return `su nombre es ${this.name}`}
}

person.name = 'juan';
console.log(person.descriptions())
console.log(Object.keys(person));
console.log(Object.values(person));

console.log('===================================================================================');
console.log(' Herencia::CAPA 2 ');
console.log('===================================================================================');

// hereda del objeto persona, sus propiedades y metodos

var alumno = {
    __proto__ : person,
    colegio : null,
};
alumno.name = 'john'
alumno.colegio = 'San juan';


console.log(alumno.descriptions()+' y estudia en '+alumno.colegio);



console.log('===================================================================================');
console.log(' FACTORIA INSTANCIAS CONSTRUCTOR ::CAPA 3 ');
console.log('===================================================================================');

/**
 *  ____________________________________                                 ____________________________________
 * |   CLASE AUTOMOBIL                  |                               |   CLASE MOTOR                      |                               
 * | ___________________________________|                               | ___________________________________|
 * | . marca                            | AUTOMOBIL hereda de MOTOR     | . potencia                         |
 * | . precio                           | ----------------------------> | . combustible                      |
 * |____________________________________|                               |____________________________________|
 * | Automovil (marca precio): Autmovil |                               | Automovil (marca precio): Autmovil |
 * | . shos(): string                   |                               | . shos(): string                   |
 * |____________________________________|                               |____________________________________|
 */

// constructor =  és una funció que produeix objectes, instàncies, d’una classe
// Es con MAYUSCULA el nombre de la funcion
function Automovil (marca,precio,potencia,combustible){
    // CONSTRUCTOR --inicio--
    this.marca = marca;
    this.precio = precio;

    // HERENCIA
    this.__proto__ = new motor(potencia,combustible);

     // PROPIEDADES Y METODOS  ==> PRIVADOS (se declaran dentro y con var) sintaxis : var propiedad, var metodo()
    var modelo = '';

     // PROPIEDADES Y METODOS  ==> PRIVADOS (se declaran dentro y con var) sintaxis : var propiedad, var metodo()
     // var printModel =  () => { // con las funciones flechas no se pierde el contexto
    var that = this;        
    var printModel = function (){
        console.log(`el modelo del coche ${that.marca} es ${that.modelo}, adquirido en el Consecionario ${Automovil._codigo_zona}`);
    };

    // PROPIEDADES Y METODOS  ==> ESTATICOS sintaxis: className._propiedad_Estatica, className._metodo_Estatico
    Automovil._codigo_zona = 123456789;
    Automovil._mensaje = 'Concesionario'

    this.show =  ()=> {
        return printModel()+'y '+ this.detalles();
    }

} // CONSTRUCTOR --fin--

// clase heredada
function motor (potencia, combustible){
    this.potencia = potencia;
    this.combustible = combustible;
    this.detalles = ()=>{
        console.log(`Con potencia de ${this.potencia} y tipo de combustible ${this.combustible}`);
    }
}


// crear una instancia de Automovil
var seat = new Automovil('seat',12,1800,'gasolina');
// introduciendo valores a atributos privados
seat.modelo = 'ibiza';
seat.show();
// saber a que objecto pertenece
console.log(Object.getPrototypeOf(seat));
// seat.printModel(); // KO los metodos privados no se pueden acceder desde fuera, solo utilizar dentro de la funcion

