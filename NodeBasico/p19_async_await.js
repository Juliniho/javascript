/**
 * Async = promesa
 * Con esto me evito las promesa encadenadas .then() return b => .then(b) return RESULTADO
 */

// funcion promesa
let getNombre = function() {

    // llamar a un error para las pruebas
    //  throw new Error(`no existe un nombre para el usuario`)  o tambien
    //  undefined.nombre   Esto da un error de JS ningun objecto de JS con propiedad undefined no es posible

    // necesario esta funcion debe tener un return y declarar la promesa
    return new Promise((resolve, reject) => {
        resolve('juan')
    })
}

let saludo = async function() {

    // esto es lo mismo del .then // espera un respuesta
    let nombre = await getNombre()

    // necesario esta funcion debe tener un return para el async
    return `Que PASA !! ${nombre}`
}

/**
 * MAIN
 */

saludo().then(mensaje => {
    console.log(mensaje);
}).catch(e => {
    console.log(`error de la funcion`, e);
})