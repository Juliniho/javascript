console.log('=============================================================================');
console.log(' Promesas ');
console.log('=============================================================================');

/**
 * Esencialmente, una promesa es un objeto devuelto al cuál se adjuntan funciones callback, 
 * en lugar de pasar callbacks a una función. 
 * 
 * Esto nos lleva a hablar del estado de una promesa, básicamente existen 3 posibles estados.

Pendiente
Resuelta
Rechazada

Una promesa originalmente esta Pendiente. Cuando llamamos a resolve entonces la promesa pasa 
a estar Resuelta, si llamamos a reject pasa a estar Rechazada, usualmente cuando es rechazada 
obtenemos un error que nos va a indicar la razón del rechazo. Cuando una promesa se resuelve 
entonces se ejecuta la función que pasamos al método .then, si la promesa es rechazada entonces 
se ejecuta la función que pasamos a .catch, de esta forma podemos controlar el flujo de datos.
 */

// ============================   Ejemplo BASICO ======================================= 
// =======================================================================================
// sin funcion flecha 
var promesa = new Promise(function(resolve, reject) {

    // exito 
    resolve('exito en la ejecucion');
    // error 'solo se imprimira si ocurre un error en el resolve'
    reject('ha ocurrido un error')
})

/**
 * @then => permite optener la respuesta de la promesa, con 2 parametros resolve,reject
 * la guardamos en una funcion anonima o flecha con el parametro @resultado o @error
 * IMPORTANTE controlar el error como excepcion sino da problemas al output, como lo estamos 
 * haciendo es este caso
 */

// EJECUCION DE LA PROMESA , DECOMENTAR PARA VER
/** 
promesa.then(function(resultado){
    console.log(resultado);
    // controlando el error, exception 
},function(error){
    console.log(error);
})
*/

// ============================   Ejemplo BASICO - MEDIO ======================================= 
// =============================================================================================

function getUser() {
    // Implementando con promesas
    return new Promise((resolve, reject) => {
        // Sin el return ni con promesas
        setTimeout(() => {
            console.log('getUser')
            resolve();
        }, 5000)
    })
};

function getProject() {
    // Implementando con promesas
    return new Promise((resolve, reject) => {
        // Sin el return ni con promesas
        setTimeout(() => {
            console.log('getProject')
            resolve();
        }, 3000)
    })
};

function getIssues() {
    // Implementando con promesas
    return new Promise((resolve, reject) => {
        // Sin el return ni con promesas
        setTimeout(() => {
            console.log('getIssues')
            resolve();
        }, 1000)
    })
}

// EJECUCION DE LA PROMESA , DECOMENTAR PARA VER

/** Ejecucion Sincrona
getUser();
getProject();
getIssues();
*/

/** Ejecucion con Promesas = method the, ASYNC encadenado que quiero que se ejecuten 

getUser().
    then(getProject).
    then(getIssues).
    catch(err =>{
        console.log(err);
    })
*/
// ============================   Ejemplo Normal ======================================= 
// =======================================================================================

var suma = (a, b) => {
    return new Promise((resolve, reject) => {
        console.log('... espera 3 segundos');
        setTimeout(() => {
            let res = a + b;
            if (res <= 5) {
                resolve(res);
            }
            reject('ERROR el resultado es mayor a 5 ')
        }, 3000)
    })
}

// EJECUCION DE LA PROMESA , DECOMENTAR PARA VER
/*
suma(1,1).then((resultado)=>{
    console.log(resultado);    
},(error)=>{
    console.log(error);
})
*/

// ============================   Ejemplo avanzado ======================================= 
// =======================================================================================

var fs = require('fs');
var file = 'ejemploAvanzado.txt';
var newFile = 'ejemploAvanzadoFicheroCreado.txt'

function leerFichero(archivo) {
    return new Promise((resolve, reject) => {
        fs.readFile(archivo, (err, data) => {
            if (err) {
                return reject(err);
            } else {
                // log de CONTROL DE EJECUCION
                console.log('leerFichero::resolve');
                return resolve(data);
            }
        })
    })
}

function escribirNuevoFichero(contenido, newFile) {
    return new Promise((resolve, reject) => {
        fs.writeFile(newFile, contenido, (err) => {
            if (err) throw reject(err);
            // log de CONTROL DE EJECUCION
            console.log('escribirNuevoFichero::resolve');
            resolve();
        })
    })
}

// EJECUCION DE LA PROMESA , DECOMENTAR PARA VER, PARA EJECUTAR 1 vez
/**
leerFichero(file)
.then(function(contenidoRespuestaLeerFichero){
    escribirNuevoFichero(contenidoRespuestaLeerFichero,newFile)
})
.catch(function(error){
    console.log('ha ocurrido un error en la ejecucion '+ error);
})
 */

// EJECUCION DE LA PROMESA , DECOMENTAR PARA VER, PARA EJECUTAR MAS DE 1 vez con "all" y 
// da 1 ARRAY DE RESPUESTA cuando 
/**
 
Promise.all([
    leerFichero(file),
    leerFichero(newFile)
]).then(respuestas => console.log(respuestas.length))
*/

// EJECUCION DE LA PROMESA , DECOMENTAR PARA VER, PARA EJECUTAR MAS DE 1 vez con "race" y 
// da 1 respuesta por cada lectura
/**
Promise.race([
    leerFichero(file),
    leerFichero(newFile)
]).then(respuesta => console.log(respuesta.toString()))
*/