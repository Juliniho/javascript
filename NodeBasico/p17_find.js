console.log('=============================================================================');
console.log(' find ');
console.log('=============================================================================');

/**
 * FIND , devolvera el objeto cuando sea TRUE, sino undefined, ver en el ejemplo
 */

var numeros = [1,2,3,4,5]
var fFind0 = numeros.find(show => show === 3)

// EJECUCION DE FIND , DECOMENTAR PARA VER
/*
console.log(fFind0);
*/

// ============================   Ejemplo  ======================================= 
// =======================================================================================

// lista de objetos
var list = [
    {nombre:'juan',apellido:'cardenas'},
    {nombre:'luis',apellido:'caceres'},
    {nombre:'pepe',apellido:'manel'},
    {nombre:'pedro',apellido:'deza'}
]

var fFind1 = list.find((see) => {
    if(see.nombre === 'luis'){
        return see.nombre
    }else{
        return console.log('no esta el objeto');
        
    };
})

// EJECUCION DE FIND , DECOMENTAR PARA VER
/*
console.log(fFind1);
*/
