/**
 * a1 sin VAR tiene un ambito de scope GLOBAL
 * @var sirve para definir el ambito de la variable para contexto
 * @const sirve para definir un valor constante a una variable
 * @let sirve para definir el valor dentro de un ambito 
 * sin var son globales dentro de una funcion
 */

function outerFoo(){
    //scope de outerFoo.

    function innerFoo(){
        //scope de outerFoo + scope de innerFoo
    }; //end scope innerFoo

    function innerFoo2(){
    //scope de outerFoo + scope de innerFoo2
    }; //end scope innerFoo2

    function innerFoo3(){
        //scope de outerFoo + scope de innerFoo3
        
        function innerInnerFoo3(){
            //scope de outerFoo + scope de innerFoo3 + scope de innerInnerFoo.
        }; //end scope innerInnerFoo3

    }; //end scope innerFoo3

}; //end scope outerFoo

console.log('=============================================================================');
console.log('============================ SCOPE VARIABLES ================================');

a1 = 1; // a sin var delante GLOBAL
var a2 = 2; // con var GLOBAL
const a3 = 3; // CONSTANTE GLOBAL
let a4 = 4; // GLOBAL 

function scope1(){
    a1 = 200; // GLOBAL declarada fuera de scope1, cambia el valor dentro
    b1 = 3; // GLOBAL para toda la pila, sin var, declarada dentro de scope1
    a4 = 5000; // OK se cambia el valor, porque tiene ambito GLOBAL
    var b2 = 4; // LOCAL solo para el AMBITO DE SCOPE1 y SCOPE3
    let b3 = 3; // LOCAL solo para el AMBITO DE SCOPE1 y SCOPE3
    // a3 = 5; // KO CONSTANTE no se puede cambiar su valor
    console.log(`f scope1 => a1 sin var: ${a1} - a2 con var: ${a2}`); // OK
    // console.log(`f scope1 => f scope3 => ${c}`); // KO no esta definida dentro del scope 1
    var scope3 = function (){
        b2 += 10;
        a1 += 1000; // OK AMBITO GLOBAL
        var c = 1000000;
        console.log(`f scope3 => dentro de f scope1 => incrementa a1 a 1000: ${a1}`); // OK AMBITO GLOBAL
        console.log(`f scope3 => dentro de f scope1 => b2 es incrementado a 10: ${b2}`); // OK AMBITO GLOBAL
        console.log(`f scope1 => f scope3 => ${c}`); 
    }
    scope3();
    scope2();
};

function scope2(){
    console.log(`f scope2 => a1 sin var: ${a1} - a2 con var: ${a2}`); // OK
    console.log(`f scope1 => ejecutado en f scope2 => b1 sin var: ${b1}`); // OK
    console.log(`f scope1 => a4 con declarada con let fuera =>a4: ${a4}`); // OK    
    // console.log(`f scope1 => ejecutado en f scope2 => b1 con var: ${b2}`); // KO fuera del ambito
    // console.log(`f scope1 => ejecutado en f scope2 => b1 con var: ${b3}`); // KO fuera del ambito
};

scope1();









