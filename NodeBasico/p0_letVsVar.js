/**
 * Diferencias entre var y let
 */


// var como variable es reutilizable sin importar el SCOPE
// deja inicializarla n veces
var nombre = 'gambito'
var nombre = 'tormenta'
if (true) {
    var nombre = 'magneto'
}


// let no deja inicialiar. solo cambiar el valor
let name = 'gambito'
    //letname = 'tormenta'
name = 'tormenta'

// let deja inicializar por estar en ambitos-scopes diferentes
if (true) {
    // se llama igual pero apunta a lugares de MEMO diferentes
    let name = 'magneto'
}


for (var i = 0; i <= 5; i++) {
    console.log(`i: ${i}`);
}



for (let x = 0; x <= 5; x++) {
    console.log(`i: ${x}`);
}


console.log(nombre);
console.log(name);

console.log(i); // SCOPE ES GLOBAL result:6

// dara error porque la x ha sido inicializada dentro
// en otro ambito por let, por mas que la inicializamos
let x;
console.log(x); // SIN valor undefined