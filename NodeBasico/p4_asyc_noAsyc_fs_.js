/**
 * CREATE FILES
 * 
fs.appendFile()
El método fs.appendFile () agrega contenido especificado a un archivo. Si el archivo no existe, se creará el archivo:
fs.appendFile('mynewfile1.txt', 'Hello content!', function (err) {
  if (err) throw err;
  console.log('Saved!');
});



fs.open()
El método fs.open () toma una "bandera" como segundo argumento, si la bandera es "w" para "escritura", el archivo especificado se abre para escritura. Si el archivo no existe, se crea un archivo vacío:
fs.open('mynewfile2.txt', 'w', function (err, file) {
  if (err) throw err;
  console.log('Saved!');
});


fs.writeFile()
El método reemplaza el archivo y contenido especificados si existe. Si el archivo no existe, se creará un nuevo archivo que contenga el contenido especificado:
fs.writeFile('mynewfile3.txt', 'Hello content!', function (err) {
  if (err) throw err;
  console.log('Saved!');
});

* DELETE FILES
The fs.unlink() method deletes the specified file:
fs.unlink('mynewfile2.txt', function (err) {
  if (err) throw err;
  console.log('File deleted!');
});

* READ FILES
fs.readFile(filename, [encoding], [callback])
fs.readFile('input.txt', function (err, data) {
   if (err) {
      return console.error(err);
   }
   console.log("Asynchronous read: " + data.toString());
});

 */


const fs = require('fs');
const ruta = 'p4_ficheroASYNC.txt'

// ===============================================================================================================

/* crea los ficheros fichero en modo sincrono**/
console.log('fs.writeFile => crea los fichero');
var textAsync = 'Este es el contenido del fichero async'
var textsync = 'Este es el contenido del fichero sync'

/* procesamiento sync **/
var temp0 = fs.writeFileSync(ruta, textAsync);
var temp1 = fs.writeFileSync('p4_ficheroSYNC.txt', textsync);
if (typeof(temp0) === 'undefined' && typeof(temp1) === 'undefined') {
    console.log('se ha creado el fichero p4_ficheroSYNC.txt y p4_ficheroASYNC.txt con exito');
} else {
    console.log('ERROR NO se ha podido crear el fichero');
}

// ===============================================================================================================


/*  Procesamiento Asincrono
    lee el contenido del fichero y lo muestra por consola*/

console.log('==============START proceso asincrono==============');

fs.readFile(ruta, (err, data) => {
    if (err) throw err;
    console.log(`Lectura p4: ${data}`)
    console.log('step1')
})
console.log('==============FIN proceso asincrono==============');

// =====================================

/* procesamiento sincrono */
console.log('==============START proceso asincrono==============');

var temp = fs.readFileSync('p4_ficheroSYNC.txt', 'utf8');
console.log(`Lectura p4: ${temp}`);
console.log('step2')

console.log('==============FIN proceso asincrono==============');

// ===============================================================================================================

console.log('==============Hace copias de ficheros==============');
/* copia de los ficheros de manera asincrona*/
fs.createReadStream('p4_ficheroASYNC.txt').pipe(fs.createWriteStream('COPIA_p4_ficheroASYNC.txt'));
fs.createReadStream('p4_ficheroSYNC.txt').pipe(fs.createWriteStream('COPIA_p4_ficheroSYNC.txt'));


// ===============================================================================================================
/* __dirname : da el directorio actual 
 *  readdir da listado del directorio actual 
 **/
console.log('==============listar direcctorio actual==============');
fs.readdir(__dirname, (err, data) => {
    if (err) throw err;
    data.forEach(element => {
        console.log(`- ${element}`);
    });
})


// ===============================================================================================================
/* 
 * note : se debe crear un fichero antes de borrarlo.
 **/

console.log('==============borrar ficheros ==============');
fs.unlink('p4_ficheroASYNC', (err) => {
    if (err) throw err;
    console.log('el archivo COPIA_p4_ficheroASYNC ha sido eliminado');
})