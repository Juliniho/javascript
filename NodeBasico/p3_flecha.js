/**
 * LAS funciones flecha arrow =>
 */
// la misma funcion pero con funcion flecha
function normal(a, b) {
    return a + b
}

let flecha = (a, b) => {
    return a + b;
}

// si la funcion tiene una linea, se puede hacer asi
let arrow = (a, b) => a + b;

let sinArgun = () => {
    return `sin argunmentos`
}

let a = a => a;

console.log(normal(2, 3));
console.log(flecha(2, 3));
console.log(arrow(2, 3));
console.log(sinArgun());
console.log(a(5));

// ==============================================
var greating = function saludar() {
    return 'funcion saludar';
}

var saludos = greating();

// DECOMENTAR PARA VER LA EJECUCION
/** 
console.log(greating); // [Function: saludar]
console.log(greating()); // dentro de saludar
console.log(saludos); // dentro de saludar
*/

// ============================================

var add = function sumar(a, b) {
    return a + b;
}

var resultSumar = add(1, 2);
// DECOMENTAR PARA VER LA EJECUCION
/** 
console.log(resultSumar); // 3
*/

// ============================================

var less = (a, b) => {
    return a - b;
}

var resultRestar = less(13, resultSumar);
// DECOMENTAR PARA VER LA EJECUCION
/** 
console.log(resultRestar); // 10
*/

// ===========================================

var incrementa = (a) => a += 3;
var resultIncrementar = incrementa(resultRestar);
// DECOMENTAR PARA VER LA EJECUCION
/** 
console.log(resultIncrementar); // 13
*/

// ===========================================

var decrementa = a => a -= 10;
var resultDecrementar = decrementa(resultIncrementar)
    // DECOMENTAR PARA VER LA EJECUCION
    /** 
    console.log(resultDecrementar); // 3
    */

// ===========================================

// setTimeout(callback, delay[, ...args])

// DECOMENTAR PARA VER LA EJECUCION
/** 
setTimeout(() => {
    return console.log(`despues de 2 segundos, imprime la llamada a la ${saludos}`);
}, 2000)
*/