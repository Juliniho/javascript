# Debug Node

* Ejecutar el debug `observar el curso de la line >52 donde señala la pos`
> node inspect <nombre_js>

    50 // continuació, però fent que NodeJS esperi entre 5 i 10 mil·lisegons entre punt i punt.
    51
    >52 const http = require('http')
    53
    54 // function serverFunction(request, response) {

`setBreakpoint('script.js', 1), sb(...): Set breakpoint on first line of script.js` te lleva a linea directamente
> debug>setBreakpoint(20)


* Siguiente linea
> n

    96 }
    97
    > 98 http.createServer(serverFunction).listen(8081)
    99

* interprete cmd para ejecutarlo `ir metiendo var o funciones para saber su valor`, para cerrar ctrl+c y volver a debug>
> repl 

* Ejecuta hasta el final o `ir al punto debugger`
> c

* BreackPoint , se escribe en el codigo `debugger`, luego node inspect <nombre_js> y finalmente c


