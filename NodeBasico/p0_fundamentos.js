/**
 * Aqui mostramos el orden de la ejecucion de la app 
 * para entender porque no es secuencial, con callbacks
 */

console.log('inicio app');

setTimeout((() => {
    console.log(`1º timeout`);
}), 3000)


setTimeout((() => {
    console.log(`2º timeout`);
}), 0)


setTimeout((() => {
    console.log(`3º timeout`);
}), 0)

// ciclo de vida de node
let saludo = function(cadena) {
    let msg = cadena
    return msg
}

console.log(saludo("hola muy buenas"));

console.log(`fin app`);