// DICCIONARIOS OBJECTOS


var dict = []
dict['nombre'] = "juan";
dict['nombre'] = "luis";
dict['nombre'] = "jaime";
for (const key in dict) {
    console.log(dict[key]); // jaime
}

var object = {}
object['nombre'] = "juan";
object['nombre'] = "luis";
object['nombre'] = "Carlos";
for (const key in object) {
    console.log(object[key]); // carlos
}

console.log(dict) // [ nombre: 'jaime' ]
console.log(typeof(dict));
console.log(object) // { nombre: 'Carlos' }
console.log(typeof(object));

console.log("///////////////////////////////////////////////////////////////////////////////////////")

var test = {};
test["cat"] = 100;
test[200] = "bird";
test["blue"] = ["red", "frog"];

console.log(test); // { '200': 'bird', cat: 100, blue: [ 'red', 'frog' ] }
console.log(Object.keys(test)) // [ '200', 'cat', 'blue' ]
console.log(Object.values(test)) // [ 'bird', 100, [ 'red', 'frog' ] ]

console.log("///////////////////////////////////////////////////////////////////////////////////////")

test = { "js": 1, "python": 0, "perl": -1 };
result = Object.keys(test)
for (var i = 0; i < result.length; i++) {
    console.log("KEY: " + result[i]);
}

console.log("///////////////////////////////////////////////////////////////////////////////////////")

list = [{ "js": 1 }, { "python": 0 }, { "perl": -1 }]
list.map((i) => {
    console.log(i);
})

console.log("///////////////////////////////////////////////////////////////////////////////////////")

var input = [{ key: "key1", value: "value1" }, { key: "key2", value: "value2" }];
var result = {};
for (var i = 0; i < input.length; i++) {
    result[input[i].key] = input[i].value; // { key1: 'value1', key2: 'value2' }
    //console.log(input[i]); // { key: 'key1', value: 'value1' } { key: 'key2', value: 'value2' }
    //console.log(Object.keys(input[i])); // [ 'key', 'value' ] [ 'key', 'value' ]
    //console.log(Object.values(input[i])) // [ 'key1', 'value1'] [ 'key2', 'value2' ]
}
console.log(result); // { key1: 'value1', key2: 'value2' }