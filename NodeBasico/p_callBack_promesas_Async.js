/**
 * Explicacion :: callbacks
 */

let empleados = [{
        id: 1,
        name: 'juan'
    },
    {
        id: 2,
        name: 'mauricio'
    },
    {
        id: 3,
        name: 'toreto'
    }
];

let salarios = [{
        id: 1,
        salario: 20000
    },
    {
        id: 2,
        salario: 23000
    }
];

let getEmpleados = function(id, callback) {
    let empleadosDB = empleados.find((empleado) => {
        // find itera todo el objeto y si coincide con el id te devuelve el result
        return empleado.id === id;
    })

    // console.log(typeof(empleadosDB));
    if (empleadosDB) {
        callback(null, empleadosDB);
    } else {
        callback(`El registro ${id} no existe en Empleados`)
    }
}

let getSalarios = (empleado, callback3) => {
    let salarioDB = salarios.find((salary) => {
        return salary.id === empleado.id;
    })

    // Object.values(Objeto), AYUDA A IMPRIMIR SI QUEREMOS SABER EL VALOR DE LOS OBJETOS
    // console.log("aqui " + Object.values(salarioDB));

    if (!salarioDB) {
        callback3(`El registro ${empleado.id} no existe en Salarios`)
    } else {
        // el primer parametro del callback3 es un null, porque no envia el error
        callback3(null, {
            nombre: empleado.name,
            salario: salarioDB.salario
        })
    }


}

// MAIN
// llamada a los metodos, ATENCION CON EL EFECTO SPAGUETTI DEL CALLBACK
// CALLBACK 1
getEmpleados(4, (err, registro) => {
    // controlamos el error
    if (!registro) {
        return console.log(err);
    }
    // CALLBACK 2
    getSalarios(registro, (err, data) => {
        if (!data) {
            return console.log(err);
        }
        console.log(data)
            // CALLBACK N
    })
})

//========================================================================================================================================


/**
 * Explicacion :: promesas arreglando el problema de los callbacks
 */

// BBDD por ejemplo 

let empleados = [{
        id: 1,
        name: 'juan'
    },
    {
        id: 2,
        name: 'mauricio'
    },
    {
        id: 3,
        name: 'toreto'
    }
];

let salarios = [{
        id: 1,
        salario: 20000
    },
    {
        id: 2,
        salario: 23000
    }
];

// sin callback, solo el parametro id
// let getEmpleados = function(id, callback) {

let getEmpleados = function(id) {
    // creamos un objeto promesas :: ( 1 call, 2 call) => 3 call
    // resolve :: exito  , reject :: error    Los nombres es un Estandar
    return new Promise((resolve, reject) => {

        let empleadoDB = empleados.find(empleado => {
            return empleado.id === id
        })

        if (!empleadoDB) {
            reject(`No existe el id ${id} en la bbdd empleados`)
        } else {
            // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
            resolve(empleadoDB)
        }
    })
}

let getSalarios = function(empleadosObject) {
    return new Promise((resolve, reject) => {

        let salarioDB = salarios.find(salario => {
            return salario.id === empleadosObject.id
        })

        if (salarioDB) {
            resolve({
                nombre: empleadosObject.name,
                sueldo: salarioDB.salario
            })

        } else {
            // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
            reject(`No existe el id ${empleadosObject.id} en la bbdd salarios`)
        }
    })
}


// utilizamos la promesa, llamamos a la funcion y con el methodo then () llamamos a resolve y reject

// promesa sola
/**
getEmpleados(1).then(
employed => {
    console.log(`El registro dentro de la bbdd pertence a ${employed.name}`);
}, err => {
    console.log(err);
})
*/

// MAIN
// promesa encadenada
getEmpleados(10).then(employed => {
        // le enviamos al siguiente then la respuesta de esta promesa con el return
        return getSalarios(employed)
    })
    .then(respuesta => {
        console.log(`El registro dentro de la bbdd pertence a ${respuesta.nombre} y tiene el salario de ${respuesta.sueldo}`)
    })
    .catch(err => {
        console.log(err);
    })


//========================================================================================================================================

/**
 * Explicacion :: Evitar el encadenamiento en promesas
 */

// BBDD por ejemplo 

let empleados = [{
        id: 1,
        name: 'juan'
    },
    {
        id: 2,
        name: 'mauricio'
    },
    {
        id: 3,
        name: 'toreto'
    }
];

let salarios = [{
        id: 1,
        salario: 20000
    },
    {
        id: 2,
        salario: 23000
    }
];



// let getEmpleados = function(id) {
let getEmpleados = async(id) => {

    // return new Promise((resolve, reject) => {

    let empleadoDB = empleados.find(empleado => {
        return empleado.id === id
    })

    if (!empleadoDB) {
        // reject(`No existe el id ${id} en la bbdd empleados`)
        throw Error(`No existe el id ${id} en la bbdd empleados`)
    } else {
        // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
        // resolve(empleadoDB)
        return empleadoDB
    }
}



// let getSalarios = function(empleadosObject) {

let getSalarios = async function(empleadosObject) {

    // return new Promise((resolve, reject) => {

    let salarioDB = salarios.find(salario => {
        return salario.id === empleadosObject.id
    })

    if (salarioDB) {
        // resolve({
        return {
            nombre: empleadosObject.name,
            sueldo: salarioDB.salario
        }

    } else {
        // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
        // reject(`No existe el id ${empleadosObject.id} en la bbdd salarios`)
        throw Error(`No existe el id ${empleadosObject.id} en la bbdd salarios`)
    }

}

// UTILIZANDO EL AWAIT - ASYNC, la mejor manera es realizar un nuevo metodo que recoja los awaits
let getInformacion = async(id) => {

    // SI lo dejamos asi, no dira que nos devuelve una promesa si no hemos cambiado la funcion getEmpleados definida como PROMESA
    // idEmpleado = getEmpleados(id)  // Promise { { id: 1, name: 'juan' } }
    idEmpleado = await getEmpleados(id) // { id: 1, name: 'juan' } 
    idsalario = await getSalarios(idEmpleado);

    // debe existir un return para devolerlo por el metodo .then()
    return `la informacion es la siguiente: el usuario ${idsalario.nombre} tiene el salario de ${idsalario.sueldo}`;
}

/**
 * MAIN
 */

getInformacion(1).then(info => {
    console.log(info);
}).catch(err => {
    console.log(err);
})