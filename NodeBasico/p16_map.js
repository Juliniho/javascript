console.log('=============================================================================');
console.log(' map ');
console.log('=============================================================================');

/**
 * MAP permite crear un nuevo array en memoria y lo recorre a la vez
 */

var numeros = [1,2,3,4,5]

// EJECUCION DE MAP , DECOMENTAR PARA VER
/*
// El metodo map recibe una funcion flecha anonima y se ejecuta tantas veces 
// cuantos elementos tenga el array
var fmap = numeros.map(show => console.log(show))
*/

// ============================   Ejemplo  ======================================= 
// =======================================================================================

// lista de objetos
var list = [
    {nombre:'juan',apellido:'cardenas'},
    {nombre:'luis',apellido:'caceres'},
    {nombre:'pepe',apellido:'manel'},
    {nombre:'pedro',apellido:'deza'}
]

// EJECUCION DE MAP , DECOMENTAR PARA VER
/*
console.log(list);
console.log(list.length);
console.log(list[0].nombre);

for (var i = 0 ; i < list.length ; i++){
    console.log(list[i].nombre, list[i].apellido);
}

// con MAP
var fmap = list.map ( see => {
    return console.log(see , see.nombre , see.apellido);
})
*/
