/**
 * lodash -> permite trabajar de manera mas comoda con .jsons u objetos
 * ver la documentacion
 */

const _ = require('lodash');

var objectCar = {}; 

console.log( typeof(objectCar));
// ================================================================================================ 
// ======================================= OBJECTOS NODE - JS =====================================
// ================================================================================================

/**
 * Objecto y sus propiedades
 */
// inicializamos un object miCar con la expresion NEW
var miCar = new Object(); 
miCar.marca = 'seat'; // asignando propiedades al object
miCar.color = 'blanco';
miCar.Año = 98;

console.log(miCar);

// ================================================================================================
// acceder a las propiedades del object asi tambien
miCar['marca'] = 'audi';
miCar.color = 'negro'

console.log(miCar);

// ================================================================================================
// Crear objecto utilizando inicializadores de objetos 
var miCarro = {
    marca : 'volvo',
    color : 'azul',
    Año : 98,
    mensaje : function (){
        console.log('welcome al object miCarro'); 
    }
} 
console.log(miCarro);

// ================================================================================================
/**
 * Crear objetos definiendo la funcion constructor
 * permite crear objetos con propiedades y metodos
 */

console.log('\n\n=================================Funcion COnstructor crear Objetos=================================');
// el nombre con MAYUSCULA POR CONVENCION
function Car (marca,color,año){
    this.marca = marca;
    this.color = color;
    this.año = año;
}

var miCarrito = new Car ('opel','verde',89);
var miCoche = new Car ('nissan','amarillo',79);

console.log(miCarrito);
console.log(miCoche);

// ================================================================================================
/**
 * Prototype
 * permite AÑADIR propiedades y metodos a una/as instacia/as ya creadas de un OBJETO no un prototipo
 */

console.log('\n\n=================================PROPOTYPE=================================');
/**
 * funcion constructor
 * @param {*} modelo 
 * @param {*} price 
 */
function Portatil (modelo,price){
    this.modelo=modelo,
    this.price=price
}

/**
 * define el tipo de objeto
 */
var acer = new Portatil('aspire',500);
console.log(acer);
var sony = new Portatil('ordelo',600);
console.log(sony);

/**
 * prototype
 * si creamos acer.color = 'negro', sony.color no funcionara ya que solo se le agrega la propiedad
 * a acer, para ello utilizamos prototype
 */
Portatil.prototype.color = 'negro';
console.log(acer.color);
console.log(sony.color);


// ================================================================================================
/**
 * Getters y Setters
 * permite crear propiedades y metodos a una/as instacia/as ya creadas
 */

console.log('\n\n=================================Getters y Setters=================================');

function IncripcionAlumno (firstName,secondName,priceMatricula,age){
    this.firstName=firstName,
    this.secondName=secondName,
    this.age=age,
    this.priceMatricula=priceMatricula,
    this.imprimirDatos= ()=>{
        return this.firstName+' '+secondName+' '+age;
    }
    /**
     * setter y getters
     */
    this.getPriceMatricula= ()=>{
        return this.priceMatricula
    }
    this.setPriceMatricula= (valor)=>{
        return this.priceMatricula=valor
    }
}

var juan = new IncripcionAlumno('juan','cardenas',2000,23);
console.log(juan);
console.log(`\n${juan.imprimirDatos()}`);
/**
 * veamos como utilizamos los getter y Setters de priceMatricula
 */
console.log(juan.getPriceMatricula());
console.log(juan.setPriceMatricula(3000));
console.log(juan.priceMatricula);


// ================================================================================================ 
// ======================================= LODASH NODE - JS =====================================
// ================================================================================================

/**
 * metodos que permite trabajar con json
 */

console.log('\n\n================================= lodash =================================');

/**
 * @var concatena : resultado de concatenar 2 objectos
 * @method assign : concatena 2 o mas objetos
 */
console.log(acer),
console.log(miCoche);
var concatena = _.assign(acer,miCoche);
console.log(concatena);

var jsonEjemplo = [
    {
      "name": "Molecule Man",
      "age": 29,
      "secretIdentity": "Dan Jukes",
      "powers": [
        "Radiation resistance",
        "Turning tiny",
        "Radiation blast"
      ]
    },
    {
      "name": "Madame",
      "age": 39,
      "secretIdentity": "Jane Wilson",
      "powers": [
        "Million tonne punch",
        "Damage resistance",
        "Superhuman reflexes"
      ]
    }
  ]


/**
 * @times : itera n veces algo
*/ 
_.times(2,()=>{
    return console.log('hola');
})

/**
 * @find : busca en un json, con mas de un parametro
 */
var resultado = _.find(jsonEjemplo,{"name": "Madame"})
console.log(resultado);

// ================================================================================================ 
// ======================================= YARGS NODE - JS =====================================
// ================================================================================================

/**
 * metodos que permite trabajar con json
 */

console.log('\n\n================================= lodash =================================');