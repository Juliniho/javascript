const http = require('http')

http.createServer ((req, res)=>{

  res.writeHead(200, { 'Content-Type': 'application/json' });
  let jsonTest = {
    nombre : 'Juan',
    edad: 33,
    url: req.url
  }

  res.write(JSON.stringify(jsonTest))
  res.end()

}).listen(1111)
console.log(`listen port 1111`);
