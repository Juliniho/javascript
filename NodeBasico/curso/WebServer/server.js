const express = require('express')
const app = express()

// Handledars
var hbs = require('hbs');

// midleware :: instruccion callback que se ejecuta siempre independiente de la url pida

// use(callback(folder static & publics)) midelware
app.use((express.static(__dirname + '/public')))
    // probar en http://localhost:3000/home.html 

// Express.js view engine for handlebars.js
hbs.registerPartials(__dirname + '/views/parcials');
app.set('view engine', 'hbs');

hbs.registerHelper("getAnio", () => {
    return new Date().getFullYear()
})

// todo lo que entre por /, ejecutan el callback (req, res), es un midleware
app.get('/', (req, res) => {

    // (1) Se puede hacer asi desde http
    // let jsonTest = {
    //   nombre : 'Juan',
    //   edad: 33,
    //   url: req.url
    // }
    // res.write(JSON.stringify(jsonTest))
    // res.end()

    // (2) o simplemente asi con express
    //res.send(jsonTest)

    // renderizara desde view/home.hbs 
    res.render('home', {
        nombre: 'juan',
    })
});

app.get('/about', (req, res) => {

    res.render('about', {

    })
});

app.listen(3000, () => {
    console.log(`listen port 3000`);
})