/**
 * pequeña app
 */

const argv = require('yargs').argv;

// DESTRUCTURACION podemos llamar dentro del objeto a un metodo
// y asi utilizarlo como constante const { ... }
// cogemos un metodo del objeto que estamos importando
// const multiplicar = require('./multiplicar/multiplicar')

const { crearArchivo } = require('./multiplicar/multiplicar_logica')

// console.log(multiplicar); // vemos que contiene el archivo importado
// podemos ir modificando la base
// let base = '10' . la comentamos para poder usar la base por parametro

// ver los ARGUMENTOS por defecto, se pasan asi : node app.js base=2
// console.log(process.argv);
// respuesta : Nos fijamos en la posicion
// [
//   '/usr/local/bin/node',
//   '/home/juan/Escritorio/javascript/NodeBasico/curso/app.js',
//   base=2
// ]

// recibimos algo por parametro y ahora sabiendo la posicion libre del process.argv, le damos la [2]
// version sin Yargs
// let argv = process.argv
// let porParametro = argv[2]
// let base = porParametro.split('=')[1] // split divide el string en una lista y le pamos la posicion

// version con Yargs

console.log(argv);


/**
* MAIN
* probamos node app.js base=5 por consola al ejecutar, se puede cambiar la base, para le version sin Yargs
*/

// Promesa
// crearArchivo(base)
//     .then(resultado => {
//         console.log('archivo creado');
//     })
//     .catch(err => {
//         console.log(err);
//     })
