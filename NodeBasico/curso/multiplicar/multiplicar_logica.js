/**
 * Aqui contendra solo la logica
 */

const fs = require('fs'); // nativos
// const fs = require('express'); // no nativos
// const fs = require('./fs') // nuestros con el path

// EXPORTANDO MULTIPLICAR.JS
// console.log(module); utilizamos para exportar el codigo, contiene module {}

let resultado = 0

// OPCION 2
// module.exports.crearArchivo // sin el let y declarando dentro del module
crearArchivo = (base) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {

            reject('no es un numero el valor introducido como base')
                // con el return cortamos la ejecucion secuencial
            return

        }

        for (let i = 0; i < 11; i++) {
            console.log(i * base);
            resultado += base * i + '\n'
                // console.log(`${base * i}`);
        }

        // modificamos el callback con la promesa
        fs.writeFile('resultado.txt', resultado, (err) => {
            if (err) {
                reject('ha ocurrido un error', err)
            }
            resolve(resultado)
        })

    })
}

// OPCION 1
module.exports = {
    // crearArchivo : crearArchivo // sin ECMS6
    crearArchivo // con ECMS6 
}