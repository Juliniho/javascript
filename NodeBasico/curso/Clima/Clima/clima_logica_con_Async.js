// para gestionar los post get
const axios = require('axios')

// CREAMOS UNA FUNCION PARA EXPORTAR LA LOGICA
const getClima = async(lat,lng) =>{

  // no necesita headers porque el API KEY lo envia como parametro en el GET
  // units=metric
  // appid=fad1f54ac3ba6ac6b71460fde02cac7b
  const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=fad1f54ac3ba6ac6b71460fde02cac7b&units=metric`)

  // siempre espera un return el async
  return res.data.main.temp;

}

// exporta la logica
module.exports = {
  getClima
}
