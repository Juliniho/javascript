// para gestionar los post get
const axios = require('axios')

// CREAMOS UNA FUNCION PARA EXPORTAR LA LOGICA
const getLugarLatLng = (direccion) =>{

  // funcion Escapada para ciudades o parametros con espacios, p.e New York
  // const encodedUlr = encodeURI(argv.dirección)
  const encodedUlr = encodeURI(direccion)

  // creamos la instancia base de la peticion
  const instance = axios.create({
    baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodedUlr}`,
    //timeout: 1000,
    headers: {'x-rapidapi-key': 'd0b2333d6cmsh48ca240cd2319e9p16de18jsncf93124e5831'}
  });


  // ejecutamos la instancia y probamos node app.js -d Arequipa
  instance.get()
  .then(resp => {
    // buscamos el algo del objeto, por eso objeto.data en este caso data
    // si vemos en postman  vemos el objeto y realmente queremos extraer
    // en este caso resp.data.Results[0]
    console.log(resp.data.Results[0]);
  })
  .catch(error => {
    console.log('Error o NOOOOO', error);
  })

  // devolvemos un objeto con las caracteriscas que queremos de la API
  return {
    dir,
    lat,
    lng
  }

}
