// para gestionar los post get
const axios = require('axios')

// CREAMOS UNA FUNCION PARA EXPORTAR LA LOGICA
const getLugarLatLng = async(direccion) =>{

  // funcion Escapada para ciudades o parametros con espacios, p.e New York
  const encodedUlr = encodeURI(direccion)

  // creamos la instancia base de la peticion
  const instance = axios.create({
    baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodedUlr}`,
    //timeout: 1000,
    headers: {'x-rapidapi-key': 'd0b2333d6cmsh48ca240cd2319e9p16de18jsncf93124e5831'}
  });

  // para el await necesitamos crear una var para que funcione como las promesas
  const res = await instance.get();

  // control
  if(res.data.Results.length === 0){
    throw new Error (`no hay resultados para ${direccion}`)
  }

  // pasamos los datos a un objeto mas pequeño de nombre
  const data = res.data.Results[0]
  const dir = data.name
  const lat = data.lat
  const lng = data.lon

  return {
    dir,
    lat,
    lng
  }
}

// exporta la logica
module.exports = {
  getLugarLatLng
}
