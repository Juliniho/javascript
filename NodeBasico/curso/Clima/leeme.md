# clima

* La sección se enfoca en los siguientes temas:

    Consumo de APIs
    Llamadas HTTP hacia servidores externos
    Paquete request
    Paquete Axios
    Uso servicios para obtener la dirección por nombre.
    Uso de OpenWeather para obtener el clima
    Respaldos locales y remotos mediante

    City Geolocation

    En las próximas clases estaremos usando una API para obtener
    las coordenadas de una ciudad en base al nombre que recibamos
    como argumento, por lo que les pido que vayan a este sitio web
    y se registren, es totalmente gratuito.

---

## Inicio

* Ejecutar la app, por consola con los siguientes parametros
> node app -d Arequipa

* inicializar proyecto
> npm init

* importaciones de paquetes
> npm i --save yargs

* configuracion de parametros con yargs p.e node app -d madrid
> ver app.js const argv = require ('yargs').options para solo pasarle el -d y nombre de ciudad

* link City Geolocation, escribes el nombre de una ciudad y te da la Geolocation, hay que logearse
> https://rapidapi.com/dev132/api/city-geo-location-lookup

  * test con postman, dara error por la API KEY
  > https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=New+York

  > https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=Arequipa

  * en heardes del postman insertar API KEY si pide el host tambien hacerlo sino no.
  > key : x-rapidapi-key
  > value : d0b2333d6cmsh48ca240cd2319e9p16de18jsncf93124e5831

* axios = promesas y http = callbacks
> npm i axios

  * tener encuenta como se ha visto en postman si utilizamos los API KEY necesitamos gestionar los hearders de la peticion, ver documentacion
  > https://www.npmjs.com/package/axios#creating-an-instance

      const instance = axios.create({
      baseURL: 'https://some-domain.com/api/',
      timeout: 1000,
      headers: {'x-rapidapi-key': 'foobar'}
      });

* API gratuita 2000 peticiones
> https://openweathermap.org/  

  * creas tu API KEY y en `https://openweathermap.org/current`, busca By geographic coordinates, probar el API KEY y el link en postman
  > api.openweathermap.org/data/2.5/weather?lat=35&lon=139

  * Junto con los parametros
    * key : appid
    * value : fad1f54ac3ba6ac6b71460fde02cac7b
