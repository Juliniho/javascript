// importa la logica
const lugar = require ('./Lugar/lugar_logica_con_Async')
const clima = require('./Clima/clima_logica_con_Async')

// pasarle parametros node app -d nombre
const argv = require ('yargs').options({
  direccion: {
    alias : 'd',
    desc :  'adresse of city carry on whether',
    demand: true
  }
}).argv

// test argv <ini>
// node .\app.js -d barcelona
// console.log(argv.dirección);
// test argv <fin>

// ----
// el objeto importado regresa una promesa porque es una funcion Async Await
// promesa ::
// lugar.getLugarLatLng(argv.direccion)
// .then(res => console.log(res))
// .catch(err => console.log(err);)

// await ::
//const coordenadas = await lugar.getLugarLatLng(argv.direccion)

// promesa = await

// clima.getClima(-12.370000,-66.279999)
// .then(resultado => console.log(resultado))
// .catch(error => console.log(error);)

// Async Await
const  getInfoClima = async( d ) =>{
    try {
    // await lat, lng
    const coordenadas = await lugar.getLugarLatLng(d)
    // await temp
    const temperatura = await clima.getClima(coordenadas.lat,coordenadas.lng)

    return `el clima de ${coordenadas.dir} tiene la temperatura ${temperatura}.`
  } catch (e) {
    return `no se pudo determinar el clima de la ciudad`
  }
}

// Promesa
getInfoClima(argv.direccion)
.then(r => console.log(r))
.catch(e => console.log(e))
