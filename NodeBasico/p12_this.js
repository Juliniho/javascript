console.log('=============================================================================');
console.log(' this ');
console.log('=============================================================================');


var mercado = {
    id : 0,
    productos : ['arroz','maiz','agua'],
    muestraProductos : function (){
        var that = this; // Se pierde la referencia por eso lo referenciamos dentro del scope
        console.log('el mercado tiene los siguientes productos: ');
        //this.productos.forEach(elementos => {  // Con funciones flecha no se pierde el This, no usar el that
        this.productos.forEach(function (elementos) {
            that.id += 1;
            console.log(`${that.id} - ${elementos}`);
        });
    }
}

mercado.muestraProductos();