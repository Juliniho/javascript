console.log('=============================================================================');
console.log(' module pattern ');
console.log('=============================================================================');

/**
 * Utilitzarem els patró de disseny module pattern i revealing module pattern, 
 * per a fer públiques les variables i/o funcions definides en un scope privat.
 */

 var mp = (()=>{
    var i = 0; // accede a i desde fuera
    return {
        incrementa : ()=>{
        return i++;
        },
        valor : ()=>{
        return console.log(i);
        }
    }
 })();

mp.incrementa();
mp.valor();
mp.incrementa();
mp.valor();

// Este patron contiene una clausura 
