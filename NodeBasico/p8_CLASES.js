class Persona {
    // constructor de la clase
    constructor (name,lastname,age){
        
        // Setters & Getters
        this._name = name;

        this.lastname = lastname;
        this.age = age;
    }

    // metodo de la clase  NUEVA SINTAXIS EN EI6
    edad (){
        return `la edad de ${this.name} es ${this.age}`;
    }

    // getters
    get name(){
        return this._name;
    }

    // setters
    set name(newName){
        this._name = newName;
    }


}

// herencia
class Alumno extends Persona {

    constructor (name,lastname,age,colegio){
        super(name,lastname,age);
        this.colegio = colegio;
    }

    imprime(){
        return `${this.edad()} y su colegio es ${this.colegio}`;
    }

    static getClassName () {
        return 'Class Alumno';
    }

}

// crear instancia 
var juan = new Alumno('juan','cardenas',12,'San Juan');
// hereda de Persona
console.log(juan.edad());
// metodo static
console.log(Alumno.getClassName());



