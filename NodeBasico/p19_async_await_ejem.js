// =============================================================================
// =============================================================================
// =============================================================================
// ASYNC - AWAIT 
// =============================================================================
// =============================================================================
// =============================================================================

/**
 * Explicacion :: Evitar el encadenamiento en promesas
 */

// BBDD por ejemplo 

let empleados = [{
        id: 1,
        name: 'juan'
    },
    {
        id: 2,
        name: 'mauricio'
    },
    {
        id: 3,
        name: 'toreto'
    }
];

let salarios = [{
        id: 1,
        salario: 20000
    },
    {
        id: 2,
        salario: 23000
    }
];



// let getEmpleados = function(id) {
let getEmpleados = async(id) => {

    // return new Promise((resolve, reject) => {

    let empleadoDB = empleados.find(empleado => {
        return empleado.id === id
    })

    if (!empleadoDB) {
        // reject(`No existe el id ${id} en la bbdd empleados`)
        throw Error(`No existe el id ${id} en la bbdd empleados`)
    } else {
        // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
        // resolve(empleadoDB)
        return empleadoDB
    }
}



// let getSalarios = function(empleadosObject) {

let getSalarios = async function(empleadosObject) {

    // return new Promise((resolve, reject) => {

    let salarioDB = salarios.find(salario => {
        return salario.id === empleadosObject.id
    })

    if (salarioDB) {
        // resolve({
        return {
            nombre: empleadosObject.name,
            sueldo: salarioDB.salario
        }

    } else {
        // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
        // reject(`No existe el id ${empleadosObject.id} en la bbdd salarios`)
        throw Error(`No existe el id ${empleadosObject.id} en la bbdd salarios`)
    }

}

// UTILIZANDO EL AWAIT - ASYNC, la mejor manera es realizar un nuevo metodo que recoja los awaits
let getInformacion = async(id) => {

    // SI lo dejamos asi, no dira que nos devuelve una promesa si no hemos cambiado la funcion getEmpleados definida como PROMESA
    // idEmpleado = getEmpleados(id)  // Promise { { id: 1, name: 'juan' } }
    idEmpleado = await getEmpleados(id) // { id: 1, name: 'juan' } 
    idsalario = await getSalarios(idEmpleado);

    // debe existir un return para devolerlo por el metodo .then()
    return `la informacion es la siguiente: el usuario ${idsalario.nombre} tiene el salario de ${idsalario.sueldo}`;
}

/**
 * MAIN
 */

getInformacion(1).then(info => {
    console.log(info);
}).catch(err => {
    console.log(err);
})