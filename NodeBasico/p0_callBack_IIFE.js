/**
 * Explicaciones
 */


// =============================================================================
// FUNCION EXPRECION 
// =============================================================================

var f0 = function f1(a, b) {
    console.log(result = a + b); // resultado de sumar los paremetros de la funcion
    // console.log(result); // OK le asigna el valor a result 
    if (result < 10) {
        var itera = f1(a + 1, b) // podemos llamar a la funcion porque estamos dentro del ambito
    } else {
        return console.log("Listo !!!");

    }
}

// DECOMENTAR PARA VER
/**
var titulo = 'FUNCION EXPRECION'
console.log(titulo);
f0(1,1) // OK funciona la llamada
// f1(1,1) // KO esta dentro del ambito 
*/

// =============================================================================
// =============================================================================
// =============================================================================
// CALLBACK 
// =============================================================================
// =============================================================================
// =============================================================================

/**
 * Explicacion Basica
 * en una funcion (var, i despues ejecuta el callback)
 * el callback es una funcion que la pasamos por param*
 */

// enviamos a una funcion flecha una var y una funcion
let getUsuarioById = (id, callback1, callback2) => {
    // un objecto
    let user1 = {
        name: 'juan',
        // id : id, si el id = id,o le pasamos el id de 
        // la func por param * ahora en EMSC se puede 
        // definir igual si tiene el mismo nombre, por redundancia
        id
    }

    // un objecto
    let user2 = {
        name: 'john',
        id: 2
    }

    if (user2.id === id) {
        console.log(`el usuario exite en bbdd user2`);
        // console.log(`datos objeto usuario 2: `, user2);
        callback2(null, user2)
    } else {
        console.log(`el usuario no existe`);
    }


    // para ver como esta el estado del objecto
    // console.log(user1);


    callback1(user1)

}

// llamada a la funcion getUsuarioById
// nameFuncion(id,callback1(param),callback2(err,param))
// DECOMENTAR PARA VER
/** 
getUsuarioById(5,
    (usuario) => {
        console.log(`El Usuario bbdd `, usuario.name);
    },
    (err, user) => {
        if (err) {
            return console.log(`erro al ejecutar el callback`, err);
        } else {
            console.log(`èl usuario es ${user.name}`);
        }
    }
)
*/

// =====================================================================================================

/**
 * Explicacion :: Problemas con 2 callbacks
 */

let empleados = [{
        id: 1,
        name: 'juan'
    },
    {
        id: 2,
        name: 'mauricio'
    },
    {
        id: 3,
        name: 'toreto'
    }
];

let salarios = [{
        id: 1,
        salario: 20000
    },
    {
        id: 2,
        salario: 23000
    }
];

let getEmpleados = function(id, callback) {
    let empleadosDB = empleados.find((empleado) => {
        // find itera rodo el objeto
        return empleado.id === id;
    })

    // console.log(typeof(empleadosDB));
    if (empleadosDB) {
        callback(null, empleadosDB);
    } else {
        callback(`El registro ${id} no existe en Empleados`)
    }
}

let getSalarios = (empleado, callback3) => {
    let salarioDB = salarios.find((salary) => {
        return salary.id === empleado.id;
    })

    // Object.values(Objeto), AYUDA A IMPRIMIR SI QUEREMOS SABER EL VALOR DE LOS OBJETOS
    // console.log("aqui " + Object.values(salarioDB));

    if (!salarioDB) {
        callback3(`El registro ${empleado.id} no existe en Salarios`)
    } else {
        // el primer parametro del callback3 es un null, porque no envia el error
        callback3(null, {
            nombre: empleado.name,
            salario: salarioDB.salario
        })
    }


}

// llamada a los metodos, ATENCION CON EL EFECTO SPAGUETTI DEL CALLBACK
// CALLBACK 1
getEmpleados(4, (err, registro) => {
    // controlamos el error
    if (!registro) {
        return console.log(err);
    }
    // CALLBACK 2
    getSalarios(registro, (err, data) => {
        if (!data) {
            return console.log(err);
        }
        console.log(data)
            // CALLBACK N
    })
})








// ====================================================================================================================================================================================
// Cuando a una funcion se le pasa como parametro a otra funcion se le llama callback

var f2 = function(a, f3) {
    var inc = f3(a);
    if (inc < 10) {
        console.log(`if => inc vale : ${inc}`);
    } else {
        console.log('else => listo inc vale: ' + inc);
    }
}

// DECOMENTAR PARA VER
/** 
// le pasamos una funcion como callback
var titulo = 'CALLBACK'
console.log(titulo);
f2(1,function(p){return p +=9;}) // define la funcion f3 y se la pasa por parametro
*/

// ====================================== Ejemplo ==============================================
// =============================================================================================

var user = {
    nombre: 'juan',
    edad: 25
}

// Esta funcion se va a EJECUTAR cuando se invoque a probarCallback
function esperaImprime(data) {
    console.log(`Dentro de esperaImprime ${data}`);

}

// callback puede ser cualquier funcion, pero tiene que ser una funcion
function probarCallback(data, callback) {
    // pasar el object a JSON
    let usuario = JSON.stringify(data);
    console.log(`Dentro de probarCallback ${usuario}`)
        // comprobar si es una funcion, el parametro que le hemos pasado
    typeof(callback) == 'function' ? callback(usuario): console.log('error no es una funcion');
}

// DECOMENTAR PARA VER, FUNCIONAMIENTO CALLBACK
/** 
// le pasamos una funcion como callback
var titulo = 'CALLBACK'
console.log(titulo);
probarCallback(user, esperaImprime);
*/




// =============================================================================
// FUNCION IIFE - SCOPE PRIVADO
// =============================================================================

// IMPORTANTE, ESTAS FUNCIONES NO SON ACCESIBLES DESDE FUERA SCOPE ES LOCAL

// DECOMENTAR PARA VER, FUNCIONAMIENTO CALLBACK 
/** 
var titulo = 'IIFE - SCOPE PRIVADO'
console.log(titulo);
(()=>{
    console.log('auto invocadodebe estar la f dentro de parentesis y acabar con ()');
})();
*/