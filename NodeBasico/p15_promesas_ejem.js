// =============================================================================
// =============================================================================
// =============================================================================
// Promesas 
// =============================================================================
// =============================================================================
// =============================================================================

/**
 * Explicacion :: Problemas con 2 Pero con promesas arreglando el problema de los callbacks
 */

// BBDD por ejemplo 

let empleados = [{
        id: 1,
        name: 'juan'
    },
    {
        id: 2,
        name: 'mauricio'
    },
    {
        id: 3,
        name: 'toreto'
    }
];

let salarios = [{
        id: 1,
        salario: 20000
    },
    {
        id: 2,
        salario: 23000
    }
];

// sin callback, solo el parametro id
// let getEmpleados = function(id, callback) {

let getEmpleados = function(id) {
    // creamos un objeto promesas :: ( 1 call, 2 call) => 3 call
    // resolve :: exito  , reject :: error    Los nombres es un Estandar
    return new Promise((resolve, reject) => {

        let empleadoDB = empleados.find(empleado => {
            return empleado.id === id
        })

        if (!empleadoDB) {
            reject(`No existe el id ${id} en la bbdd empleados`)
        } else {
            // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
            resolve(empleadoDB)
        }
    })
}

let getSalarios = function(empleadosObject) {
    return new Promise((resolve, reject) => {

        let salarioDB = salarios.find(salario => {
            return salario.id === empleadosObject.id
        })

        if (salarioDB) {
            resolve({
                nombre: empleadosObject.name,
                sueldo: salarioDB.salario
            })

        } else {
            // resolve solo admite un parametro, en el caso de querer enviar mas parametros, enviarlos como objetos { name : objeto.propiedades}
            reject(`No existe el id ${empleadosObject.id} en la bbdd salarios`)
        }
    })
}


// utilizamos la promesa, llamamos a la funcion y con el methodo then () llamamos a resolve y reject

// promesa sola
/**
getEmpleados(1).then(
    employed => {
        console.log(`El registro dentro de la bbdd pertence a ${employed.name}`);
    }, err => {
        console.log(err);
    })
*/

// promesa encadenada
getEmpleados(10).then(employed => {
        // le enviamos al siguiente then la respuesta de esta promesa
        return getSalarios(employed)
    })
    .then(respuesta => {
        console.log(`El registro dentro de la bbdd pertence a ${respuesta.nombre} y tiene el salario de ${respuesta.sueldo}`)
    })
    .catch(err => {
        console.log(err);
    })