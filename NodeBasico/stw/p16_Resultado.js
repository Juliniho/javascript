// 16. Definim un objecte de “tipus future” com un objecte de dos camps tal i com es mostra a continuació

const fs = require('fs');

function readIntoFuture(file){
  
  fs.readFile(file,'UTF-8',(err,data)=>{
    if(err) throw err
    future.isDone = true
    future.result = data
  })

  return future = {
    // ens indica si l’operació ja ha acabat
    // inicialment és false, i passa a ser true quan l’operació ha acabat.
    isDone : false,
    // El camp result és null mentre isDone == false
    // i conté el resultat de l’operació quan aquesta ja ha acabat..
    result : null,
  }
}


// MAIN

// caso 1
futuru = readIntoFuture('a1.txt')
console.log(future);

// caso 2
futuru = readIntoFuture('a1.txt')
setTimeout(()=>{
  console.log(future);
},1000)
