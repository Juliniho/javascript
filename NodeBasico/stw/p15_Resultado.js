/**
 * 15. Fes una nova classe, DecreasingCounter, que estengui l’anterior per herència i que faci que el mètode inc
en realitat decrementi el comptador.
 */

//   DecreasingCounter <--Counter // tiene que devolver 0 no?¿?¿?¿

function DecreasingCounter() {

    // publico para ser compartido
    this.dec = (number) => {
        // necesita el return sino no devuelve nada
        return --number
    }
}

function Counter() {

    // herencia
    this.__proto__ = new DecreasingCounter()

    // privado
    var count = 1

    // privado
    var notify = null

    // Setter
    this.setNotify = (f) => {
        notify = f
    }

    this.inc = () => {
        if (count < 1) {
            notify = ''
        } else {
            notify(this.__proto__.dec(count))
        }
    }
}

/**
 * MAIN
 */

var o3 = new Counter()

o3.setNotify(function(a) { console.log(a) })
o3.inc()