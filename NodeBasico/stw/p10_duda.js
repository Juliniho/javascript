/**
 * 10. Explica perquè, a l’exercici anterior, hi podria haver problemes si en comptes de fer això
llista.forEach(function (element, index) {  ...  } )
* féssim això altre
* var index = 0
* llista.forEach(function (element) {  ...  index += 1} )
* Indicació: ...suposant que fem servir index al callback de fs.readFile.
 */


// duda si es que hay que 
// El tema es que la clausura de la variable "i" fuera del forEach, que hace que sea la misma siempre. Te falta, 
// por ejemplo, un private scope para copiar la variable i en una distinta en cada iteración.

function createFile(file) {
    file.map((files) => {
        fs.writeFile(files, 'contigut ' + files, function(err) {
            if (err) throw err;
            console.log(`creado el archivo ${files}`);
        });
    })
}




let f7 = function(llista, callback_final) {

    let index = 0
    let l = []

    console.log(`la lista contiene ${llista.length} elementos y el orde es: ${llista}`);

    llista.forEach((file) => {

        console.log(`aqui ` + index);


        fs.readFile(file, 'utf8', (err, data) => {

            if (err) {
                callback_final(`existe un error`)
            }

            l[index] = data
            index++

            // Creo que ya esta solucionado

            console.log(`aqui ` + index);

            l.length > 3 ? callback_final(null, l) : ''

        })

        // data = fs.readFileSync(file)
        // console.log(data.toString());
        // console.log(i);
        // lsync[i] = data
        // i++
        // lsync.length > 3 ? callback_final(null, lsync) : ''
    });

}

/**
 * MAIN
 */

let llista = ['a1.txt', 'a2.txt', 'a3.txt', 'a4.txt']

createFile(llista)

setTimeout((() => {
    f7(llista, (err, resultado) => {
        if (err) {
            console.log(err);
        }
        console.log(resultado);
    })
}), 2000)