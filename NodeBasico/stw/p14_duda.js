/**
 * 14. Converteix l’exemple anterior en una classe i assigna’l a un objecte o3. En que es diferencien els dos exemples?
El següent codi és un exemple d’una classe que pots reaprofitar:
function Counter() {
this.a = 1;
this.inc = function () { this.a++; },
this.count = function() { return this.a; }
}
new Counter()
 */

// duda En que es diferencien els dos exemples?
// esta tambien 'class Persona'

function Counter() {

    // privado
    var count = 1

    // privado
    var notify = null

    // Setter
    this.setNotify = (f) => {
        notify = f
    }

    this.inc = () => {
        if (count < 1) {
            notify = ''
        } else {
            notify(++count)
        }
    }
}


/**
 * MAIN
 */



var o3 = new Counter()

o3.setNotify(function(a) { console.log(a) })
o3.inc()