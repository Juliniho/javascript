/**
 * 13. Fes el mateix que a l’exercici anterior però fes servir el module pattern per amagar el valor del comptador i la funció
especificada a notify. Fes un setter per la funció triada per l’usuari. Anomena l’objecte com o2.
El següent codi és un exemple de module pattern que pots reaprofitar:
var testModule = (function() {
var count = 1;
return {
inc : function() { count++; },
count: function() { return count; }
};
})();
Exemple d’us:
o2.setNotify(function (a) { console.log(a) }); o2.inc()
Resultat:
2
 */

// module pattern 
let o2 = (function() {

    // scope private

    let count = 1
    notify = null

    return {

        // scope public : call scope private

        inc: () => {
            if (count < 1) {
                notify = ''
            } else {
                notify(++count)
            }
        },

        setNotify: (f) => {
            notify = f
        }
    }

})()


/**
 * MAIN
 */


o2.setNotify(function(a) { console.log(a) })
o2.inc()