const fs = require('fs');

function asyncToFuture(f) {
  let future = {
     isDone : false,
     result : null
  }
  return function(p){
    future.isDone = true
    future.result = f(p)
    // console.log(future);
  }
}

function asyncToEnhancedFuture(f) {
  let future = {
     isDone : false,
     result : null,
     registerCallback : (f)=>{
       this.isDone = true
       return function (p){
         f(p)
       }
    }
  }
  return function(p){
    f(p)
    console.log(future);
  }
}

// MAIN
let  readIntoEnhancedFuture = asyncToEnhancedFuture((p)=>{
  fs.readFile(p,'UTF-8',(err,data)=>{
    if(err) throw err
    // console.log(data)
  })
})

let enhancedFuture = readIntoEnhancedFuture('a1.txt')
// enhancedFuture.registerCallback( function() {console.log('hi')})

// INPUT { isDone: false, result: null, registerCallback: [Function: registerCallback]} ??
// TEST { isDone: true, result: ’contingut 1’, registerCallback: [Function] }
