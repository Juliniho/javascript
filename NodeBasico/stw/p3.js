/**
 * 3. Crea una funció f3 que rebi una llista com a primer paràmetre i retorni una llista.
llista2 = f3(llista)
Cada element y de llista2 ha de ser el resultat d’aplicar
f2(x) + 23
a cada element x de llista, on f2 és la funció de l’exercici anterior.
Exemple d’us:
f3([1,2,3])
Resultat:
[25,27,29]
 */

// const f2 = require('./p2');


let f2 = (a) => {
    a = 2 * a
    return a * 2 >= 0 ? a : -1;
}

// IMPORTANTE EL RETURN 

let f3 = (llista) => {
    return llista.map((lista) => {
        return f2(lista) + 23
    })
}

console.log(f3([1, 2, 3]));