const fs = require('fs');

function asyncToFuture(f) {
  let future = {
     isDone : false,
     result : null
  }
  return function(p){
    future.isDone = true
    future.result = f(p)
    // console.log(future);
  }
}

// MAIN
let  readIntoFuture2 = asyncToFuture((p)=>{
  fs.readFile(p,'UTF-8',(err,data)=>{
    if(err) throw err
  })
})

let future = readIntoFuture2('a1.txt')
setTimeout(()=>{
  console.log(future);
},1000)

// INPUT { isDone: true, result: undefined } ??
// TEST { isDone: true, result: <Buffer 63 6f 6e 74 69 6e 67 75 74 20 31 0a> }
