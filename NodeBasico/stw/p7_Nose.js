/**
 * 7. Afegeix una nova funció printaki2 a l’objecte console que imprimeixi per consola “aqui 1”, “aqui 2”, “aqui
3”, etc. És a dir “aqui {número anterior + 1}”. No facis servir cap variable global (ni cap objecte global) per guardar
el comptador; fes servir una clausura.
Exemple d’us:
console.printaki2()
Resultat:
aqui 1
Exemple d’us:
console.printaki2()
Resultat:
aqui 2
 */

var f = function() {
    let acomulado = 0
    return () => {
        console.log(`aqui ${acomulado++}`)
    }
}

let f1 = f()
console.printaki2 = f1

console.printaki2()
console.printaki2()
console.printaki2()