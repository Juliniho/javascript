/**
 * 4. Afegeix una nova funció printaki a l’objecte console que imprimeixi “aqui” per consola.
Exemple d’us:
console.printaki()
Resultat:
aqui
 */

// duda , es sobre el objeto consola o creamos uno que se llame asi?¿

let consola = {
    printaki: () => {
        console.log(`aqui`);
    },

    // otra manera

    printaki2() {
        console.log(`aqui`);
    }
}

consola.printaki2()