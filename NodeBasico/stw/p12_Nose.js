/**
 * 12. Fes un objecte o1 amb tres propietats: un comptador count, una funció inc que incrementi el comptador, i una
variable notify que contindrà null o bé una funció d’un paràmetre. Feu que el comptador “notifiqui” la funció
guardada a la propietat notify cada cop que el comptador s’incrementi.
Exemple d’us:
o1.notify = null; o1.inc()
Resultat:

Exemple d’us:
o1.count = 1; o1.notify = function(a) { console.log(a) }; o1.inc()
Resultat:
2
 */

let o1 = {
    count: 0,
    inc: function() {
        if (this.count < 1) {
            this.notify = ''
        } else {
            this.notify(++this.count)
        }
    },
    notify: null
}

/**
 * MAIN
 */


// o1.notify = null;
// o1.inc()

o1.count = 1
o1.notify = function(a) { console.log(a) }
o1.inc()