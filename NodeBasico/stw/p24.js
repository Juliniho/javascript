
/////////////////////////////////////////////////////////////

// (1)

var p = Promise.reject(0)
p.then() // resolve

setTimeout(()=>{
  p.catch(err => {}) // reject, dara error, noejecuta la f() dentro timeout
}, ms: number)

// (2)

p.catch(err => {}) // si que se ejecuta


/////////////////////////////////////////////////////////////
// da resultados distintos por que esta en el mismo evento o linea

var p = Promise.reject(0) ; p.catch(console.log)
