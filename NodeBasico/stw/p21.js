const composer = (fx,fy)=>{
  return function(p){
      console.log(fx(fy(p)))
  }
}

let f1 = (a)=>{ return a+1 }
let f3 = composer(f1,f1)
f3(3)

let f4 = (a)=>{ return a*3 }
let f5 = composer(f3,f4)
f5(3)
