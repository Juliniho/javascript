/**
 * 2. Crea una funció f2 que rebi un paràmetre a i que retorni 2 * a si a >= 0 i -1 en cas contrari.
Exemple d’us:
f2(3)
Resultat:
6
Exemple d’us:
f2(-2)
Resultat:
-1
 */

// Operador condicional (ternario) condición ? expr1 : expr2


let f2 = (a) => {
    a = 2 * a
    console.log(a * 2 >= 0 ? a : -1)
}

f2(3);
f2(-2)

// module.exports = f2