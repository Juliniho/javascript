/**
 * 1. Crea una funció f1 que rebi un paràmetre a i que l’escrigui a la consola fent servir la funció log de l’objecte
console.
Exemple d’us:
f1(3)
Resultat:
3
 */

f1 = a => console.log(a)

f1(3)