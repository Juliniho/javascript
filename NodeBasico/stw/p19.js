const fs = require('fs');

// VERSION AYUDA

// let when = function () {
//   return {
//     a : console.log('hola')
//   }
// }
//
// when().a // hola

let when = function(f){
  return {
    do : f
  }
}

// MAIN

f1 = function(callback) { fs.readFile('a1.txt','UTF-8', callback) }
f2 = function(error, result) { console.log(result) }

debugger
when(f1).do(f2)
