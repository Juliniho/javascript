var enhancedFutureToPromise = function(enhancedFuture){

  return new Promise(
    (resolve , reject) =>{
      enhancedFuture.registerCallback((ef)=> {
        resolve(ef.result)
      })
    }
  )
}

var asyncToEnhancedFuture = function(f) {
  return(filename) => {
    let callback = null;
    var resFuture = {
      isDone: false,
      result: null,
      registerCallback: function(p) {
        callback = p;
      }
    };

    f(filename, function(err, res) {
      future.isDone = true;
      future.result = res;
      if (!callback) {
        callback(fresFuture)
      }
    })
    return resFuture;
  }
}
