function Tokenizer() {

    this.dictionary = []

    var d = null

    this.run = (llista) => {
        llista.forEach((keys) => {
            // console.log(keys);
            if (keys in this.dictionary) {
                // console.log(`El caracter esta`);
                this.dictionary[keys]()
            } else {
                // console.log(`no esta`);
                d()

            }
        })
    }


    this.on = (c, f) => {
        this.dictionary[c] = f
    }


    this.onDefault = (f) => {
        d = f
    }

}


function testTokenizer() {

    // Creeu un objecte de tipus Tokenizer .
    var t = new Tokenizer();
    var countA = 0;
    var countC = 0;
    var countOtros = 0;
    var testString = ['H', 'o', 'l', 'a', ' ', 'c', 'o', 'm', ' ', 'a', 'n', 'e', 'u', '?'];

    // Registreu al Tokenizer una funció que mantingui un comptador del nombre de caràcters 'a' que ha vist.
    t.on('a', () => {
        ++countA
    });

    t.on('c', () => {
        ++countC
    });

    t.on('', () => {
        ++countA
    });

    t.onDefault(() => {
        ++countOtros
    });

    // Feu que el Tokenizer examini la llista a la variable testString de l'esquelet.
    t.run(testString)

    // here goes the code to run the test over testString

    console.log("numero de a's: " + countA);
    console.log("numero de c's: " + countC);
    console.log("numero d'altres caracters: " + countOtros)
}

testTokenizer();