/** 
 * 6. Crea una funció f5 que agafi un objecte i dues funcions per paràmetres, anomenats respectivament a, b, i c:
f5 = function(a, b, c) {
// a és un objecte (valor, variable etc), b és una funció, i c és una funció.
}
La funció f5 ha d’aplicar la funció b a l’objecte a. El resultat se li ha de passar a c. La funció c ha de ser una
funció callback amb un paràmetre (i.e., aquesta funció s’ha de cridar quan la feina que faci f5 s’hagi acabat, i el
resultat de la feina se li ha de passar com a paràmetre).
Exemple d’us:
f5(1, f2, function(r) { console.log(r) })
Resultat:
2
*/

let f2 = (a) => {
    a = 2 * a
    return a * 2 >= 0 ? a : -1
}

let f5 = (a, b, callback) => {
    callback(null, b(a))
}

f5(1, f2, (err, r) => {
    if (err) {
        console.log(`ha ocurrido un error`);
    }
    console.log(r);
})