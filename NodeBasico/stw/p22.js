const asyncComposer = (fx,fy)=>{
  return function(p,call){
      fx(p,call)
  }
}

let f1 = (a,callback)=>{ callback(null,a+1) }
let f3 = asyncComposer(f1,f1)
f3(3, function( err, resultado) {
  console.log(resultado);
})
