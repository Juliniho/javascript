/**
 * 9. Modifica la funció f6 de l’exercici anterior perquè l’ordre de la llista resultat coincideixi amb l’ordre orig-
inal de llista. És a dir que a cada posició resultat[i] hi ha d’haver el contingut de l’arxiu anomenat a
llista[i]. Anomena aquesta funció modificada com f7.
Fes servir llista.forEach(function (element, index) {} ).
Exemple d’us:
f7([’a1.txt’,’a2.txt’], function (result) { console.log(result) })
Resultat:
[’contingut a1.txt’, ’contingut a2.txt’]
 */

// duda , si el orden de inserccion se debe al index de llista.forEach((file, index) => {?¿

const fs = require('fs');

function createFile(file) {
    file.map((files) => {
        fs.writeFile(files, 'contigut ' + files, function(err) {
            if (err) throw err;
            console.log(`creado el archivo ${files}`);
        });
    })
}

function deleteFile(name) {
    name.map(file => {
        fs.unlink(file, function(err) {
            if (err) throw err;
            console.log('Borrado');
        });
    })
}


let f7 = function(llista, callback_final) {

    let l = []

    // console.log(`la lista contiene ${llista.length} elementos y el orde es: ${llista}`);

    llista.forEach((file, index) => {

        fs.readFile(file, 'utf8', (err, data) => {
            if (err) throw err

            l[index] = data
            l.length > 3 ? callback_final(null, l) : ''
        })
    });
}

/**
 * MAIN
 */

let llista = ['a1.txt', 'a2.txt', 'a3.txt', 'a4.txt']

createFile(llista)

setTimeout((() => {
    f7(llista, (err, resultado) => {
        if (err) {
            console.log(err);
        }
        console.log(resultado);
    })
}), 2000)


// setTimeout((() => {
//     deleteFile(llista)
// }), 5000)