/**
 * 11. Implementa la funció asyncMap. Aquesta funció té la següent convenció d’us:
 *  function asyncMap(list, f, callback2) {...}
    function callback2(err, resultList) {...}
    function f(a, callback1) {...}
    function callback1(err, result) {...}
    Fixa’t en que f(...) té la mateixa forma que fs.readFile(...).
    La funció asyncMap aplica f a cada element de list i crida callback2 quan acaba d’aplicar f a tots els
    elements de la llista. La funció callback2 s’ha de cridar, o bé amb el primer err != null que se li hagi
    passat a callback1, o bé amb resultList contenint el resultat de la feina feta per asyncMap (en l’ordre
    correcte).
    Indicació: fixa’t en els paral·lelismes entre aquest exercici i els anteriors. Intenta entendre que vol dir fer un map
    ası́ncron.
    Exemple d’us:
    asyncMap([’a1.txt’], fs.readFile, function (a, b) { console.log(b) })
    Resultat:
    [’contingut 1’]
 */
const fs = require('fs');

function createFile(file) {
    file.map((files) => {
        fs.writeFile(files, 'contigut ' + files, function(err) {
            if (err) throw err;
            console.log(`creado el archivo ${files}`);
        });
    })
}


// =========== y el CALLBACK 1 ?¿?¿?¿?¿?¿?¿?¿?


let asyncMap = (list, f, callback2) => {

    list.map(l => {
        f(l, 'utf8', (err, data) => {
            if (err) {
                callback2(err)
            }
            callback2(null, data)
        })
    })

}

/**
 * MAIN
 */

let list = ['a1.txt']

createFile(list)

setTimeout(() => {

    asyncMap(list, fs.readFile, (a, b) => {
        console.log(b);
    })

}, 2000)