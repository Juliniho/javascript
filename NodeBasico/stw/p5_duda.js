/**
 * 5. Primer fes una funció f4 que sumi dos números (i.e., f4(a,b) → a + b), i fes una llista
llistaA = [1,2,3,4]
Fes servir llistaB = llistaA.map(...) per sumar 23 a cada element de llistaA, i fes servir f4 per
fer la suma. Indicació: et caldrà fer una funció addicional, ja que no es pot fer servir f4 directament.
Exemple d’us:
llistaB
Resultat:
[24,25,26,27]
 */

// esta bien ?¿?¿

let f4 = (a, b) => { return a + b }
let llistaA = [1, 2, 3, 4]

let llistaB = llistaA.map(l => {
    return f4(l, 23)
})

console.log(llistaB); //[ 24, 25, 26, 27 ] NOSE SI ES COMO LO PIDE EL ENUNCIADO