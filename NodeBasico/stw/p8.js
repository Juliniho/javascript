/**
 * 8. Crea una funció f6 que tingui dos paràmetres: una llista de noms d’arxiu i una funció callback.
f6 = function(llista, callback_final) { ... }
La funció f6 ha de llegir els arxius anomenats a llista i ha de crear una nova llista resultat amb el contingut
d’aquests arxius. I.e., cada element de resultat ha de ser el contingut d’un dels arxius.
Fes servir .forEach(...) i fs.readFile(filename, callback).
Quan la funció hagi acabat de llegir tots els arxius, s’ha de cridar la funció callback que f6 rep com a paràmetre
(i.e., callback_final) amb la llista resultant. Fixa’t en que quan s’ha cridat l’última funció callback passada
a fs.readFile(...) és quan realment s’han acabat de llegir tots els arxius. Fixa’t també en que l’última
callback del fs.readFile que s’executa no té perquè ser la que se li ha donat al fs.readFile a l’última
iteració del .forEach.
Nota: el resultat no té perquè seguir l’ordre correcte (depèn de quan vagin acabant els fs.readFile).
Exemple d’us:
f6([’a1.txt’,’a2.txt’], function (result) { console.log(result) })
Resultat:
[’contingut a2.txt’, ’contingut a1.txt’]
 */

const fs = require('fs');

function createFile(file) {
    file.map((files) => {
        fs.writeFile(files, 'contigut ' + files, function(err) {
            if (err) throw err;
            console.log(`creado el archivo ${files}`);
        });
    })
}

function deleteFile(name) {
    name.map(file => {
        fs.unlink(file, function(err) {
            if (err) throw err;
            console.log('Borrado');
        });
    })
}


let f6 = function(llista, callback_final) {
    let l = []

    //console.log(`la lista contiene ${llista.length} elementos`);

    llista.map((files) => {
        fs.readFile(files, 'utf8', (err, data) => {
            if (err) throw err
            l.push(data)
                // `condición ? expr1 : expr2` 
            l.length > 1 ? callback_final(null, l) : ''
        })
    })
}

/**
 * MAIN
 */

let llista = ['a1.txt', 'a2.txt']

createFile(llista)

setTimeout((() => {
    f6(llista, (err, resultado) => {
        console.log(resultado);
    })
}), 2000)


// setTimeout((() => {
//     deleteFile(llista)
// }), 5000)