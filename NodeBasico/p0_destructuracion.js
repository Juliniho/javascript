/**
 * let {propiedades,metodos} = el objeto 
 */

let miObject = {
    nombre: 'juan',
    apellido: 'cardenas',
    porder: 'leer la mente',
    // se puede definir de esta manera las funciones
    getNombre() {
        return `su nombre es ${this.nombre}`
    },
    getApellido: function() {
        return `su apellido es ${this.apellido}`
    },
    // this, apunta a lo que vale fuera del metodo
    // por eso da undefined
    // por eso utilizar getPoder() {..}
    getPoder: () => `su poder es ${this.porder}`
}

// extraemos datos del objecto destructurandolo
// metodo normal
let nombre = miObject.nombre;
// destructurando
// propiedad : nuevo nombre
let { apellido, poder: power } = miObject;

console.log(miObject.getNombre());
console.log(miObject.getApellido());
console.log(miObject.getPoder());

console.log(nombre);
console.log(apellido);
console.log(power);