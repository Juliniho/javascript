// modulos node
const os = require('os');
const fs = require('fs');

// uso
let cpu = os.cpus();
let network = os.networkInterfaces();
let exportaValorVariable = "vale 1";

var stringObject = JSON.stringify(network);
var p1 = "C:\Users\PortatilOut\Desktop\Node\NodeBasico\p1.txt"

fs.writeFile("p1_fichero.txt",stringObject,(err)=>{
    // si sucede algun error llama a la excepcion err
    if (err) throw err;
    console.log('The file has been saved!');
});

// exportar .js
module.exports = {
    miVariable : exportaValorVariable,
    functionAnonima : function(){
        console.log('dentro de la funcion Anonima');
    },
    funcionFlecha : ()=>{
        console.log('dentro de la funcion Flecha');
    },
}


