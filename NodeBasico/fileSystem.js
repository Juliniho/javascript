var fs = require('fs');

/**
 *  renombra fs.rename(oldPath, newPath, callback), 
 *  tiene el mismo nombre para que no salte la exceptions de error
 *  renameSync => es sincrono, no tiene callback fs.renameSync(oldPath, newPath)
 * */ 
fs.rename('tempCambiado.txt','tempCambiado.txt',(err)=>{
    // excepcion de error
    if(err)throw err 
    console.log('exito con fs.rename');    
})

/**
 * fs.appendFile(),  method appends specified content to a file. If the file does not exist, 
 * the file will be created
 * fs.appendFile(filename, data[, options], callback)
• filename {String}
• data {String | Buffer}
• options {Object}
• encoding {String | Null} default = 'utf8'
 */

var contenido = 'contenido del fichero tempCambiado para usar el method, fs.rename';
fs.appendFile('tempCambiado.txt',contenido,(err)=>{
    if(err) throw err;
    console.log('exito con fs.appendFile');
}) 

/**
 * The fs.open() method takes a "flag" as the second argument, if the flag is "w" for "writing", 
 * the specified file is opened for writing.
 * If the file does not exist, an empty file is created
 * fs.open(path, flags[, mode], callback)
 * fd => De hecho, stdout, stdin y stderr también reciben un descriptor de archivo, están ocupando fd 0 a n, 
 */

 fs.open('tempCambiado.txt','w',(err,fd)=>{
     if(err)throw err;
     console.log('exito en fd de fs.open es :'+fd);
 })

 /**
  * fs.writeFile(filename, data[, options], callback)
• filename {String}
• data {String | Buffer}
• options {Object}
• encoding {String | Null} default = 'utf8'
• mode {Number} default = 438 (aka 0666 in Octal)
• flag {String} default = 'w'
• callback {Function}
Asynchronously writes data to a file, replacing the file if it already exists. data can be a string or a buffer.
The encoding option is ignored if data is a buffer. It defaults to 'utf8'.
 * fs.writeFile('message.txt', 'Hello Node.js', 'utf8', callback);  
*/ 

fs.writeFile ('tempCambiadoNewFile.txt',contenido,(err)=>{
    if(err){
        console.log('error en fs.writeFile');
    }
    console.log('exito en fs.writeFile');
})  
 
 /**
  * fs.readFile(filename[, options], callback)
• filename {String}
• options {Object}
• encoding {String | Null} default = null
• flag {String} default = 'r'
• callback {Function}
  */

  fs.readFile('tempCambiado.txt',(err,data)=>{
      if(err) throw err;
      console.log('exito fs.readfile el contenido es: '+data);
  })


