console.log('=============================================================================');
console.log('================================ CLAUSURAS ==================================');

// La clausura es esa capacidad de mantener las referencias, o englobarlas, o encerrarlas.
// una funcion A => DENTRO DE OTRA funcion B => donde la funcion B tiene acceso a las Variables a de la funcion A
// RECIBEN UNA REFERENCIA NO UN VALOR, POR TANTO => si modificamos una variable de la FUNCION PADRE- A dentro de la funcion B
// esto se aplica.
// FACTORIA
/**
 * funcion A {
 *      var a
 *      funcion B {
 *          var b
 *      }
 * }
 * 
 */

function identificador(id) {
    return function(name) {
        id += 10;
        console.log('su id es ' + id + " de : " + " " + name);
    }
}

identificador(0)('pedros Suarez')

// factoria
var consulta = identificador(10);

consulta('Juan Cardenas')
consulta('Luis Miguel')

console.log('================================ Ejemplo =========================================');

// Math.pow(base, exponente)
function raiz(exponente) {
    return function(base) {
        var result = Math.pow(base, exponente);
        return result;
    }
}

// factoria de raices
var cuadrada = raiz(2);
var cubica = raiz(3);

console.log(cuadrada(7));
console.log(cubica(3));

console.log('================================ Ejemplo 1 =========================================');

function contador() {
    var cont = 0;
    return function() {
        cont++;
        return console.log(`el contador esta aumentado a: ${cont}`);
    }
}

var cuenta = contador();
cuenta();
cuenta();

console.log('================================ Ejemplo 2 =========================================');

function incrementador(i) {
    var inc = function(num) {
        var add = i;
        console.log(`${num +=add}`);
    }
    return inc;
}

// Nos fijamos que le enviamos valor a la funcion y al objeto
var i10 = incrementador(10);

i10(5); // al 5 le incrementamos 10 = 15
i10(90); // 100