# INSTALAR
https://es.reactjs.org/docs/create-a-new-react-app.html#create-react-app

npx create-react-app my-app
cd my-app
npm start

# FICHEROS DE CONFIGURACION 
## PACKAGE - LOCK .js

> archivos de configuracion de la app

## .gitignore

> archivos que no se subiran a los repos, aqui se puede generar var de entorno, archivos para ignorar y no subir a repos

## src

> contiene el codigo de react-la app

> index.js 
  * Arranca la app de react, con el tag del componente de public/index.html ><div id="root"></div>
  * Renderiza un componente   
        * ReactDOM.render(<App />, document.getElementById('root')); // 
  * Tambien contiene librerias necesarias 
        * import React fr'react'; 
        * import ReactDOM from 'react-dom';
        * import './index.css'
        * import App from './App'; 

> App.js
  * contiene EL CODIGO JSX , DEFINIMOS AQUI LOS COMPONENTES html para en JS | DONDE CREAMOS LOS COMPONETES
        * P.e Class = ClassName
  * ir a https://babeljs.io/repl probar escribir en htmls | genera code JS (react)
        * <h1>Hello world</h1> = React.createElement("h1", null, "Hello world"); su biblioteca hace que esto no tenga que escribirse en todo el codigo
 
## public

> contiene el template html ,archivo/s para montar la app, es el div que contiene el id del proyecto/app
* por ejemplo en index.html contiene el <div id="root"></div> 
> si el usuario desea deshabilita el JS UTILIZAR ESTE TAG </noscript>
* <noscript>You need to enable JavaScript to run this app.</noscript>

# Conceptos
## props 
> Propiedades definidas como objetos que se les pasan a las funciones dentro de un componente 

        {
            text : 'texto'
        }

        function Saludo(props){
        // los componentes siempre retornan algo
        return (
            <div id="hello"> Este es el COMPONENTE SALUDO y esta declarado como un tag saludo que es JSX 
            Este es el COMPONENTE SALUDO y esta declarado como un tag saludo que es JSX 
            <br></br>
            {props.text}
            <h3>{props.otro}</h3>
            </div>
        )
        }

        function App() {
        return (
            <div className="App">
                
                <Saludo  text="valor de  props en el componente saludo " otro="otro props"></Saludo>
