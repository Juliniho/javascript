import React from 'react'; // IMPORTAR REACT,PARA CREAR LA INTERFAST CON LA FUNCION APP ()
import logo from './logo.svg'; // IMPORTA EL LOGO
import './App.css';

// CREAR COMPONENTE, luego se le aplica el tag con su nombre en la funcion APP = componente

// como funcion flecha
const ComponenteFunFlecha = () => <div>Componente definido con la FUNCION FLECHA</div>

// como clase, pero se tiene que heredar metodos de React porque necesita el return
class ComoClase extends React.Component{
  render(){
    return <div>Componenente Definido como una CLASE</div>
  }
}

// como una funcion normal
function Saludo(props){
  // los componentes siempre retornan algo
  return (
    <div id="hello"> 
      Este es el COMPONENTE SALUDO y esta declarado como un tag saludo que es JSX 
      <br></br>
      {props.text}
      <h3>{props.otro}</h3>
    </div>
  )
}

function App() {
  return (
    <div className="App">
  
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <Saludo  text="valor de  props en el componente saludo " otro="otro props"></Saludo>
        <ComponenteFunFlecha></ComponenteFunFlecha>
        <ComoClase></ComoClase>

        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

      </header>



    </div>
  );
}

export default App;
