import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// Tecnologia de la progres WEBAPP
import * as serviceWorker from './serviceWorker';


// Renderiza el componente, en este caso lo que esta dentro del div id root
// ReactDOM.render(<nombre del componente>, utiliza un metodo de JS para decir donde lo hara con el ID);
ReactDOM.render(<App />, document.getElementById('root'));


serviceWorker.unregister();
